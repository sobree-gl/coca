<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Recipe extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('DBrecord');
        
        $this->id = end($this->uri->segment_array());
        $this->table = 'operations_recipe';

	}

    private function seo()
	{
		$title          = "Operations / Recipe";
		$robots         = "noindex,nofollow";
		$description    = "titlewebtitleweb";
		$keywords       = "titleweb,titleweb";
		$meta  			= '<TITLE>'.$title.'</TITLE>';
		$meta 		   .= '<meta name="robots" content="'.$robots.'"/>';
		$meta		   .= '<meta name="description" content="'.$description.'"/>';
        $meta 		   .= '<meta name="keywords" content="'.$keywords.'"/>';
        $meta 		   .= '<meta property="og:url" content="'.site_url().'" />';
        $meta 		   .= '<meta property="og:type" content="website" />';
        $meta 		   .= '<meta property="og:title" content="'.$title.'" />';
        $meta 		   .= '<meta property="og:description" content="'.$description.'" />';
        $meta 		   .= '<meta property="og:image" content="'.base_url('image/logo/logo.png').'" />';
		return $meta;
    }

    private function SiteURL($SiteURL)
	{
		$SiteURL = site_url('operations/recipe/'.$SiteURL);
        return $SiteURL;
	}
    
    private function thisURL()
	{
        $sess_data['id'] = $this->id;
        $sess_data['link'] = current_url();
        $this->session->set_userdata('Recipe',$sess_data);
    }

    private function upload_image($file, $path) {
		$config['upload_path'] = $path;
		$config['allowed_types'] = '*';
		$config['max_size'] = '*';
		$config['max_width']  = '*';
		$config['max_height']  = '*';
		$config['encrypt_name']  = TRUE;
		$this->load->library('upload', $config);
		if(!$this->upload->do_upload($file)) {
			return array('file_name' => false); // ถ้าอัพโหลดไม่ได้ ไม่สามารถเรียกดูข้อมูลไฟล์ที่อัพได้
		}else{
            $file_name = $this->upload->data();  // ถ้าอัพโหลดได้ เราสามารถเรียกดูข้อมูลไฟล์ที่อัพได้
            $this->load->library('image_lib');
            $configer =  array(
                'image_library'   => 'gd2',
                'source_image'    =>  $file_name['full_path'],
                'maintain_ratio'  =>  TRUE,
                'width'           =>  900,
                'height'          =>  900,
            );
            $this->image_lib->clear();
            $this->image_lib->initialize($configer);
            $this->image_lib->resize();

            return $file_name['file_name'];
		}
	}

	public function index()
	{
        $this->thisURL();
        $data = array(
            'seo'     => $this->seo(),
            'menu'    => 'operations',
            'header'  => 'header',
            'content' => 'operations/recipe/index',
            'footer'  => 'footer',
            'function'=>  array('operations'),
        );
        $this->load->view('template/body', $data);
    }

    public function filter()
	{
        $this->thisURL();
        $data = array(
            'seo'     => $this->seo(),
            'menu'    => 'operations',
            'header'  => 'header',
            'content' => 'operations/recipe/filter',
            'footer'  => 'footer',
            'function'=>  array('operations'),
        );
        // DBrecord //
        $DBrecord['id'] = $this->id;
        $DBrecord['table'] = $this->table;
        $data['operations_recipe'] = $this->DBrecord->get_result2($DBrecord);
        // DBrecord //
        $data['Urladd'] = $this->SiteURL('form/add');
        $data['Urledit'] = $this->SiteURL('form');
        $data['Urldelete'] = $this->SiteURL('delete');
        $this->load->view('template/body', $data);
    }

    public function detail()
	{
        $this->thisURL();
        $data = array(
            'seo'     => $this->seo(),
            'menu'    => 'operations',
            'header'  => 'header',
            'content' => 'operations/recipe/detail',
            'footer'  => 'footer',
            'function'=>  array('operations'),
        );
        $this->load->view('template/body', $data);
    }
    
    public function form()
	{
        $data = array(
            'seo'     => $this->seo(),
            'menu'    => 'operations',
            'header'  => 'header',
            'content' => 'operations/recipe/form',
            'footer'  => 'footer',
            'function'=>  array('operations'),
        );
        // DBrecord //
        $DBrecord['id'] = array('recipeID' =>  end($this->uri->segment_array()));
        $DBrecord['table'] = $this->table;
        $data['result'] = $this->DBrecord->get_first($DBrecord);
        // DBrecord //
        if (end($this->uri->segment_array())=='add') {
            $data['Urlform'] = $this->SiteURL('create');
        } else {
            $data['Urlform'] = $this->SiteURL('update');
        }
        
        $this->load->view('template/body', $data);
    }
    
    public function _build_data($input)
	{
        $value['sort'] = $input['sort'];
        $value['title'] = $input['title'];
        $value['branchID'] = implode(',', $input['branchID']);

        if ($input['id']==null) {
            $value['createDate'] = date('Y-m-d H:i:s');
            $value['createBy'] = $this->session->sess_login['usersID'];
        } else {
            $value['updateDate'] = date('Y-m-d H:i:s');
            $value['updateBy'] = $this->session->sess_login['usersID'];
        }
        return $value;
    }

    public function create()
	{
        // DBrecord //
        $input = $this->input->post();
        $value = $this->_build_data($input);
        
        $value['image'] = $input['file_img_hid'];
        if (isset($_FILES['file_img']['name']) && !empty($_FILES['file_img']['name'])) {
            $value['image'] = $this->upload_image('file_img', './uploads/operations/recipe/');
        }

        $DBrecord['value'] = $value;
        $DBrecord['table'] = $this->table;
        $this->DBrecord->insert($DBrecord);
        // // DBrecord //
        redirect( $this->session->Recipe['link'], 'refresh');
    }
    
    public function update()
	{
        // DBrecord //
        $input = $this->input->post();
        $value = $this->_build_data($input);

        $value['image'] = $input['file_img_hid'];
        if (isset($_FILES['file_img']['name']) && !empty($_FILES['file_img']['name'])) {
            $value['image'] = $this->upload_image('file_img', './uploads/operations/recipe/');
            if ($value['image']) {
                unlink('./uploads/operations/recipe/'.$input['file_img_hid']);
            }
        }

        $DBrecord['id'] = array('recipeID'=>$input['recipeID']);
        $DBrecord['value'] = $value;
        $DBrecord['table'] = $this->table;
        $this->DBrecord->update($DBrecord);
        // DBrecord //
        redirect( $this->session->Recipe['link'], 'refresh');
    }
    
    public function delete()
	{
        // DBrecord //
        $DBrecord['id'] = array('recipeID' =>  end($this->uri->segment_array()));
        $DBrecord['table'] = $this->table;

        $result = $this->DBrecord->get_first($DBrecord);
        if ($result->image) {
            unlink('./uploads/operations/recipe/'.$result->image);
        }

        $this->DBrecord->delete($DBrecord);
        // DBrecord //
        redirect( $this->session->Recipe['link'], 'refresh');
	}
    
}