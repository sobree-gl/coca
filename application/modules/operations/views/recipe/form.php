<!-- ============================================================== -->
<div class="page-wrapper">
    <div class="container-fluid">

        <div class="row">
            <div class="col-md-12">
                <h2>Recipe Center</h2>
                <hr><br>
                <?php echo form_open_multipart($Urlform); ?>
                <div class="form-group">
                    <div class="row">
                        <label for="title" class="col-md-2 control-label">Sort</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" id="sort" name="sort"
                                value="<?=isset($result->sort) ? $result->sort : '';?>" required>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label for="title" class="col-md-2 control-label">Title</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" id="title" name="title"
                                value="<?=isset($result->title) ? $result->title : '';?>" required>
                        </div>
                    </div>
                </div>
                <?php if($this->session->Recipe['id']==9999){ ?>
                <input type="hidden" id="branchID[]" name="branchID[]"
                    value="<?=$this->session->Recipe['id'];?>">
                <?php }else{ ?>
                <div class="form-group">
                    <div class="row">
                        <label for="title" class="col-md-2 control-label">Brand</label>
                        <div class="col-md-10">
                        <select class="select-multiple" name="branchID[]" multiple="multiple" style="width: 100% !important;">
                            <?php
                            $rr_branchID = explode(',', $result->branchID);
                            $branch = $this->db->get('branch');
                            foreach ($branch->result() as $val) {
                            ?>
                            <option value="<?=$val->branchID;?>" <?php if (in_array($val->branchID, $rr_branchID)){echo 'selected';}?>><?=$val->title;?></option>
                            <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <div class="form-group">
                    <div class="row">
                        <label for="title" class="col-md-2 control-label">Image</label>
                        <div class="col-md-10">
                        <input type="hidden" name="file_img_hid" value="<?=!empty($result->image) ? $result->image : '';?>">
                            <div class="custom-file" id="customFile" lang="es">
                                <input type="file" class="custom-file-input" name="file_img">
                                <label class="custom-file-label" for="exampleInputFile">
                                <?=!empty($result->image) ? $result->image : 'Select file...';?>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="offset-md-2 col-md-10">
                            <input type="hidden" id="recipeID" name="recipeID"
                                value="<?=isset($result->recipeID) ? $result->recipeID : '';?>">
                            <button type="submit" class="btn btn-info">SAVE</button>
                        </div>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>

        <!-- ============================================================== -->
    </div>
</div>