<!-- ============================================================== -->
<div class="page-wrapper">
    <div class="container-fluid">

        <div class="row">
            <div class="col-md-12">
                <h2>Recipe Center</h2>
                <hr>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="listdata">
                    <div class="item-category">
                        <a href="<?=site_url('operations/recipe');?>">
                            <div class="sub">Recipe Center</div>
                        </a>
                        <?php if($this->session->Recipe['id']==9999){ ?>
                        <a href="">
                            <div class="sub active">Drink</div>
                        </a>
                        <?php } ?>
                        <?php
                        $this->db->from('branch');
                        $this->db->where('branchID', $this->uri->segment(4));
                        $result = $this->db->get();
                        foreach ($result->result_array() as $value) {
                        ?>
                        <div class="sub"><?=$value['title'];?></div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="listdata">

            <div class="row">
                <div class="col-md-12">
                <?php if ($this->session->sess_login['type']=='Corporate') { ?>
                <div class="text-right mb-4">
                    <a href="<?=$Urladd;?>">
                        <button type="button" class="btn btn-success"><i class="fas fa-plus"></i> Add</button>
                    </a>
                </div>
                <?php } ?>
                    <div class="row">
                        <?php foreach ($operations_recipe as $val) { ?>
                        <div class="col-md-4 mb-4">
                            <a href="<?=site_url('operations/recipe_detail/index/'.$val->recipeID);?>">
                                <div class="item-image flex">
                                    <img src="<?=base_url('uploads/operations/recipe/'.$val->image);?>" alt="" class="img">
                                    <div class="text">
                                        <span><?=$val->title;?></span>
                                    </div>
                                </div>
                            </a>
                            <div class="text-center">
                                <?php if ($this->session->sess_login['type']=='Corporate') { ?>
                                <a href="javascript:void(0);"
                                    click-delete="<?=$Urldelete.'/'.$val->recipeID;?>">
                                    <button type="button" class="btn btn-danger"><i class="far fa-trash-alt"></i></button>
                                </a>
                                <a href="<?=$Urledit.'/'.$val->recipeID;?>">
                                    <button type="button" class="btn btn-warning"><i class="far fa-edit"></i></button>
                                </a>
                                <?php  } ?>

                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>

        </div>

        <!-- ============================================================== -->
    </div>
</div>