<!-- ============================================================== -->
<div class="page-wrapper">
    <div class="container-fluid">

        <div class="row">
            <div class="col-md-12">
                <h2>Recipe Center</h2>
                <hr>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="listdata">
                    <div class="item-category">
                        <a href="">
                            <div class="sub active">Recipe Center</div>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">

                <div class="row listdata">
                    <div class="offset-md-2 col-md-8">
                        <div class="row">
                            <?php
                            $branch = $this->db->get('branch');
                            foreach ($branch->result() as $val) {
                            ?>
                            <div class="col-md-4 mb-4">
                                <a href="<?=site_url('operations/recipe/filter/'.$val->branchID);?>">
                                    <div class="item-image">
                                        <img src="<?=base_url('uploads/branch/'.$val->image);?>" alt="" class="img">
                                    </div>
                                </a>
                            </div>
                            <?php } ?>
                            <div class="col-md-4 mb-4">
                                <a href="<?=site_url('operations/recipe/filter/9999');?>">
                                    <div class="item-image">
                                        <img src="<?=base_url('image/logo/Drink.png');?>" alt="" class="img">
                                        <div class="text">
                                            <span>Drink</span>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <!-- ============================================================== -->
    </div>
</div>