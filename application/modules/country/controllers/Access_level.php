<?php
defined('BASEPATH') OR exit('No direct script access_level allowed');

class Access_level extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('DBrecord');

        $this->id = end($this->uri->segment_array());
        $this->table = 'users_access';
    }
    


    private function seo()
	{
		$title          = "Control system / Access_level";
		$robots         = "noindex,nofollow";
		$description    = "titlewebtitleweb";
		$keywords       = "titleweb,titleweb";
		$meta  			= '<TITLE>'.$title.'</TITLE>';
		$meta 		   .= '<meta name="robots" content="'.$robots.'"/>';
		$meta		   .= '<meta name="description" content="'.$description.'"/>';
        $meta 		   .= '<meta name="keywords" content="'.$keywords.'"/>';
        $meta 		   .= '<meta property="og:url" content="'.site_url().'" />';
        $meta 		   .= '<meta property="og:type" content="website" />';
        $meta 		   .= '<meta property="og:title" content="'.$title.'" />';
        $meta 		   .= '<meta property="og:description" content="'.$description.'" />';
        $meta 		   .= '<meta property="og:image" content="'.base_url('image/logo/logo.png').'" />';
		return $meta;
    }

    private function SiteURL($SiteURL)
	{
		$SiteURL = site_url('country/access_level/'.$SiteURL);
        return $SiteURL;
	}
    
    private function thisURL()
	{
		$sess_data = array(
            'id' => $this->id,
            'link' => current_url()
        );
        $this->session->set_userdata('access_level',$sess_data);
        $this->session->set_userdata('back_users',$sess_data);
    }

	public function index()
	{
        $this->thisURL();
        $data = array(
            'seo'     => $this->seo(),
            'menu'    => 'country',
            'header'  => 'header',
            'content' => 'country/access_level/index',
            'footer'  => 'footer',
            'function'=>  array('country'),
        );
        // DBrecord //
        $DBrecord['id'] = array('level' => $this->id);
        $DBrecord['table'] = $this->table;
        $data['result'] = $this->DBrecord->get_result($DBrecord);
        // DBrecord //
        $data['Urladd'] = $this->SiteURL('form/add');
        $data['Urledit'] = $this->SiteURL('form');
        $data['Urldelete'] = $this->SiteURL('delete');
        $data['Urlaction'] = site_url('country/access_group/index');

        $DBrecord2['id'] = array('accessID' => $this->id);
        $DBrecord2['table'] = $this->table;
        $data['result2'] = $this->DBrecord->get_first($DBrecord2);

        $data['Urlaction2'] = site_url('country/users/index/Corporate');
        $this->load->view('template/body', $data);
    }
    
    public function form()
	{
        $data = array(
            'seo'     => $this->seo(),
            'menu'    => 'country',
            'header'  => 'header',
            'content' => 'country/access_level/form',
            'footer'  => 'footer',
            'function'=>  array('country'),
        );
        // DBrecord //
        $DBrecord['id'] = array('accessID' =>  end($this->uri->segment_array()));
        $DBrecord['table'] = $this->table;
        $data['result'] = $this->DBrecord->get_first($DBrecord);
        // DBrecord //
        if (end($this->uri->segment_array())=='add') {
            $data['Urlform'] = $this->SiteURL('create');
        } else {
            $data['Urlform'] = $this->SiteURL('update');
        }
        
        $this->load->view('template/body', $data);
    }
    
    public function _build_data($input)
	{
        $value['title'] = $input['title'];
        $value['level'] = $input['level'];

        if ($input['accessID']==null) {
            $value['createDate'] = date('Y-m-d H:i:s');
            $value['createBy'] = $this->session->sess_login['usersID'];
        } else {
            $value['updateDate'] = date('Y-m-d H:i:s');
            $value['updateBy'] = $this->session->sess_login['usersID'];
        }
        return $value;
    }

	public function create()
	{
        // DBrecord //
        $input = $this->input->post();
        $value = $this->_build_data($input);
        
        $DBrecord['value'] = $value;
        $DBrecord['table'] = $this->table;
        $this->DBrecord->insert($DBrecord);
        // // DBrecord //
        redirect( $this->session->access_level['link'], 'refresh');
    }
    
    public function update()
	{
        // DBrecord //
        $input = $this->input->post();
        $value = $this->_build_data($input);

        $DBrecord['id'] = array('accessID'=>$input['accessID']);
        $DBrecord['value'] = $value;
        $DBrecord['table'] = $this->table;
        $this->DBrecord->update($DBrecord);
        // DBrecord //
        redirect( $this->session->access_level['link'], 'refresh');
    }
    
    public function delete()
	{
        // DBrecord //
        $DBrecord['id'] = array('accessID' =>  end($this->uri->segment_array()));
        $DBrecord['table'] = $this->table;

        $this->DBrecord->delete($DBrecord);
        // DBrecord //
        redirect( $this->session->access_level['link'], 'refresh');
	}
    
}