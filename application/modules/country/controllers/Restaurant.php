<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Restaurant extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('DBrecord');

        $this->id = null;
        if (!empty($this->uri->segment(4))) {
            $this->id = end($this->uri->segment_array());
        }
        $this->table = 'restaurant';
    }


    private function seo()
	{
		$title          = "Restaurant";
		$robots         = "noindex,nofollow";
		$description    = "titlewebtitleweb";
		$keywords       = "titleweb,titleweb";
		$meta  			= '<TITLE>'.$title.'</TITLE>';
		$meta 		   .= '<meta name="robots" content="'.$robots.'"/>';
		$meta		   .= '<meta name="description" content="'.$description.'"/>';
        $meta 		   .= '<meta name="keywords" content="'.$keywords.'"/>';
        $meta 		   .= '<meta property="og:url" content="'.site_url().'" />';
        $meta 		   .= '<meta property="og:type" content="website" />';
        $meta 		   .= '<meta property="og:title" content="'.$title.'" />';
        $meta 		   .= '<meta property="og:description" content="'.$description.'" />';
        $meta 		   .= '<meta property="og:image" content="'.base_url('image/logo/logo.png').'" />';
		return $meta;
    }

    private function SiteURL($SiteURL)
	{
		$SiteURL = site_url('country/restaurant/'.$SiteURL);
        return $SiteURL;
	}
    
    private function thisURL()
	{
		$sess_data = array(
            'id' => $this->id,
            'link' => current_url()
        );
        $this->session->set_userdata('restaurant',$sess_data);
        $this->session->set_userdata('back_users',$sess_data);
    }
    

	public function index()
	{
        $this->thisURL();
        $data = array(
            'seo'     => $this->seo(),
            'menu'    => 'country',
            'header'  => 'header',
            'content' => 'country/restaurant/index',
            'footer'  => 'footer',
            'function'=>  array('country'),
        );
        // DBrecord //
        if (!empty($this->uri->segment(4))) {
            $arryaID['franchiseeID'] = $this->id;
        }
        if ($this->session->sess_login['type']=='Restaurant') {
            $arryaID['restaurantID'] = $this->session->sess_login['typeID'];
        }
        $DBrecord['id'] = $arryaID;
        $DBrecord['table'] = $this->table;
        $data['result'] = $this->DBrecord->get_result($DBrecord);
        // DBrecord //
        $data['Urladd'] = $this->SiteURL('form/add');
        $data['Urledit'] = $this->SiteURL('form');
        $data['Urldelete'] = $this->SiteURL('delete');
        $data['Urlaction'] = site_url('country/users/index/Restaurant');
        $this->load->view('template/body', $data);
    }
    
    public function form()
	{
        $data = array(
            'seo'     => $this->seo(),
            'menu'    => 'country',
            'header'  => 'header',
            'content' => 'country/restaurant/form',
            'footer'  => 'footer',
            'function'=>  array('country'),
        );
        // DBrecord //
        $DBrecord['id'] = array('restaurantID' =>  end($this->uri->segment_array()));
        $DBrecord['table'] = $this->table;
        $data['result'] = $this->DBrecord->get_first($DBrecord);
        $data['branch'] = $this->DBrecord->get_result(array('table' => 'branch'));
        $data['franchisee'] = $this->DBrecord->get_result(array('table' => 'franchisee'));
        $data['country'] = $this->DBrecord->get_result(array('table' => 'country'));
        // DBrecord //
        if (end($this->uri->segment_array())=='add') {
            $data['Urlform'] = $this->SiteURL('create');
        } else {
            $data['Urlform'] = $this->SiteURL('update');
        }
        
        $this->load->view('template/body', $data);
    }
    
    public function _build_data($input)
	{
        $value['title'] = $input['title'];
        $value['detail'] = $input['detail'];
        $value['branchID'] = $input['branchID'];
        $value['franchiseeID'] = $input['franchiseeID'];
        $value['countryID'] = $input['countryID'];
        $value['zoneID'] = $input['zoneID'];

        if ($input['restaurantID']==null) {
            $value['createDate'] = date('Y-m-d H:i:s');
            $value['createBy'] = $this->session->sess_login['usersID'];
        } else {
            $value['updateDate'] = date('Y-m-d H:i:s');
            $value['updateBy'] = $this->session->sess_login['usersID'];
        }
        return $value;
    }

	public function create()
	{
        // DBrecord //
        $input = $this->input->post();
        $value = $this->_build_data($input);
        
        $DBrecord['value'] = $value;
        $DBrecord['table'] = $this->table;
        $this->DBrecord->insert($DBrecord);
        // // DBrecord //
        redirect( $this->session->restaurant['link'], 'refresh');
    }
    
    public function update()
	{
        // DBrecord //
        $input = $this->input->post();
        $value = $this->_build_data($input);

        $DBrecord['id'] = array('restaurantID'=>$input['restaurantID']);
        $DBrecord['value'] = $value;
        $DBrecord['table'] = $this->table;
        $this->DBrecord->update($DBrecord);
        // DBrecord //
        redirect( $this->session->restaurant['link'], 'refresh');
    }
    
    public function delete()
	{
        // DBrecord //
        $DBrecord['id'] = array('restaurantID' =>  end($this->uri->segment_array()));
        $DBrecord['table'] = $this->table;

        $this->DBrecord->delete($DBrecord);
        // DBrecord //
        redirect( $this->session->restaurant['link'], 'refresh');
	}
    
}