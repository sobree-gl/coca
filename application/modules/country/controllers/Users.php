<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('DBrecord');
        $this->load->helper('string');
	}

    private function seo()
	{
		$title          = "Control system / Users";
		$robots         = "noindex,nofollow";
		$description    = "titlewebtitleweb";
		$keywords       = "titleweb,titleweb";
		$meta  			= '<TITLE>'.$title.'</TITLE>';
		$meta 		   .= '<meta name="robots" content="'.$robots.'"/>';
		$meta		   .= '<meta name="description" content="'.$description.'"/>';
        $meta 		   .= '<meta name="keywords" content="'.$keywords.'"/>';
        $meta 		   .= '<meta property="og:url" content="'.site_url().'" />';
        $meta 		   .= '<meta property="og:type" content="website" />';
        $meta 		   .= '<meta property="og:title" content="'.$title.'" />';
        $meta 		   .= '<meta property="og:description" content="'.$description.'" />';
        $meta 		   .= '<meta property="og:image" content="'.base_url('image/logo/logo.png').'" />';
		return $meta;
    }

    private function SiteURL($SiteURL)
	{
		$SiteURL = site_url('country/users/'.$SiteURL);
        return $SiteURL;
	}
    
    private function thisURL()
	{
		$sess_data = array(
            'id' => end($this->uri->segment_array()),
            'type' => $this->uri->segment(4),
            'link' => current_url()
        );
        $this->session->set_userdata('Users',$sess_data);
    }
    
    private function upload_image($file, $path) {
		$config['upload_path'] = $path;
		$config['allowed_types'] = '*';
		$config['max_size'] = '*';
		$config['max_width']  = '*';
		$config['max_height']  = '*';
		$config['encrypt_name']  = TRUE;
		$this->load->library('upload', $config);
		if(!$this->upload->do_upload($file)) {
			return array('file_name' => false); // ถ้าอัพโหลดไม่ได้ ไม่สามารถเรียกดูข้อมูลไฟล์ที่อัพได้
		}else{
            $file_name = $this->upload->data();  // ถ้าอัพโหลดได้ เราสามารถเรียกดูข้อมูลไฟล์ที่อัพได้
            return $file_name['file_name'];
		}
	}

    public function index()
	{
        $this->thisURL();
        $data = array(
            'seo'     => $this->seo(),
            'menu'    => 'country',
            'header'  => 'header',
            'content' => 'country/users/index',
            'footer'  => 'footer',
            'function'=>  array('country'),
        );
        
        // DBrecord //
        $data_id['typeID'] = $this->session->Users['id'];
        $data_id['type'] = null;
        if ($this->session->Users['type']) {
            $data_id['type'] = $this->session->Users['type'];
        }
        $DBrecord['table'] = 'users';
        $DBrecord['id'] = $data_id;
        $data['result'] = $this->DBrecord->get_result($DBrecord);
        // DBrecord //
        $data['Urladd'] = $this->SiteURL('form/add');
        $data['Urledit'] = $this->SiteURL('form');
        $data['Urldelete'] = $this->SiteURL('delete');
        $data['url_deactivate'] = $this->SiteURL('deactivate');
        $this->load->view('template/body', $data);
    }
    
    public function form()
	{
        $data = array(
            'seo'     => $this->seo(),
            'menu'    => 'country',
            'header'  => 'header',
            'content' => 'country/users/form',
            'footer'  => 'footer',
            'function'=>  array('country'),
        );

        $this->db->select('t1.title, t1.groupID');
        $this->db->from('users_group as t1');
        $this->db->join('users_access as t2', 't1.accessID = t2.accessID', 'LEFT');
            // ->where('users_access.level', 0)
        if ($this->session->Users['type']=='Corporate') {
            $this->db->where('t1.accessID', $this->session->Users['id']);
        }else{
            $this->db->like('t2.title', $this->session->Users['type']);
        }
        $data['result_users_group'] = $this->db->get()->result();

        // DBrecord //
        $DBrecord['id'] = array('usersID' => end($this->uri->segment_array()));
        $DBrecord['table'] = 'users';
        $data['result'] = $this->DBrecord->get_first($DBrecord);
        // DBrecord //
        if (end($this->uri->segment_array())=='add') {
            $data['Urlform'] = $this->SiteURL('create');
        } else {
            $data['Urlform'] = $this->SiteURL('update');
        }
        
        $this->load->view('template/body', $data);
    }
    
    public function _build_data($input)
	{
        // $value['username'] = $input['username'];
        $value['title'] = $input['title'];
        $value['firstname'] = $input['firstname'];
        $value['lastname'] = $input['lastname'];
        $value['position'] = $input['position'];
        $value['email'] = $input['email'];
        $value['type'] = $input['type'];
        $value['typeID'] = $input['typeID'];
        $value['groupID'] = $input['groupID'];

        $value['active'] = 1;
        $value['deactivate'] = 1;
        $value['createDate'] = $input['createDate'];

        if ($input['usersID']==null) {
            $value['createBy'] = $this->session->sess_login['usersID'];
        } else {
            $value['updateDate'] = date('Y-m-d H:i:s');
            $value['updateBy'] = $this->session->sess_login['usersID'];
        }
        return $value;
    }

	public function create()
	{

        // DBrecord //
        $input = $this->input->post();
        $value = $this->_build_data($input);

        $value['image'] = $input['file_img_hid'];
		if (isset($_FILES['file_img']['name']) && !empty($_FILES['file_img']['name'])) {
            $value['image'] = $this->upload_image('file_img', './uploads/users/');
        }

		$newpassword = random_string('alnum', 6);
        $value['password'] = base64_encode($newpassword);

        $DBrecord['value'] = $value;
        $DBrecord['table'] = 'users';
        $this->DBrecord->insert($DBrecord);
        // DBrecord //
        $this->sendmail($input['email'],$newpassword);
        redirect( $this->session->Users['link'], 'refresh');
    }
    
    public function update()
	{
        // DBrecord //
        $input = $this->input->post();
        $value = $this->_build_data($input);

        $value['image'] = $input['file_img_hid'];
        if (isset($_FILES['file_img']['name']) && !empty($_FILES['file_img']['name'])) {
            $value['image'] = $this->upload_image('file_img', './uploads/users/');
            if ($value['image']) {
                unlink('./uploads/users/'.$input['file_img_hid']);
            }
        }

        $value['password'] = base64_encode($input['password']);

        $DBrecord['id'] = array('usersID' => $input['usersID']);
        $DBrecord['value'] = $value;
        $DBrecord['table'] = 'users';
        $this->DBrecord->update($DBrecord);
        // DBrecord //
        redirect( $this->session->Users['link'], 'refresh');
    }
    
    public function delete()
	{
        // DBrecord //
        $DBrecord['id'] = array('usersID' => end($this->uri->segment_array()));
        $DBrecord['table'] = 'users';

        $result = $this->DBrecord->get_first($DBrecord);
        if ($result->image) {
            unlink('./uploads/users/'.$result->image);
        }

        $this->DBrecord->delete($DBrecord);
        // DBrecord //
        redirect( $this->session->Users['link'], 'refresh');
    }

    public function deactivate()
	{
        // DBrecord //
        $value['deactivate'] = 0;
        if ($this->uri->segment(4)==0) {
            $value['deactivate'] = 1;
        }
        $DBrecord['id'] = array('usersID' => end($this->uri->segment_array()));
        $DBrecord['value'] = $value;
        $DBrecord['table'] = 'users';
        $this->DBrecord->update($DBrecord);
        // DBrecord //
        redirect( $this->session->Users['link'], 'refresh');
    }

    private function sendmail($mails,$pass)
    {
        // Load PHPMailer library
        $this->load->library('phpmailer_lib');
        // PHPMailer object
        $mail = $this->phpmailer_lib->load();
        // SMTP configuration
        $mail->CharSet = 'UTF-8';
        $mail->isSMTP();
        $mail->SMTPDebug = 0;
        $mail->Host = "smtp.gmail.com";
        $mail->Port = "587";
        $mail->SMTPSecure = "tls";
        $mail->SMTPAuth = true;
        $mail->Username = "cocath2020@gmail.com";
        $mail->Password = "coca2020@pass";
        $mail->setFrom("cocath2020@gmail.com", "+inspired Community");
        // Set email format to HTML
        $mail->isHTML(true);
        // Add a recipient
        // $mail->addAddress('tarn_thanita@outlook.co.th');
        $mail->addAddress($mails);
        // Add cc or bcc 
        // $mail->addCC('cc@example.com');
        // $mail->addBCC('bcc@example.com');
        // Email subject
        $mail->Subject = 'Registering - #'.date('dmYHis');

        $body = "<div style='border: 1px solid rgb(7, 133, 167);border-radius: 5px;color: #fff;'>";
		$body.= "<div style='background-color: rgb(0, 151, 192);padding: 10px;text-align: center;'>";
		$body.= "<p style='margin: 0;font-size: 20px;'>Message Detail</p>";
		$body.= "</div>";
		$body.= "<div style='color: #000;padding: 10px;'>";
        $body.= "<p>Thank you for registering and welcome to +inspired Community by COCA, Mango Tree and Mango Chili! Here's your account information:</p>";
        $body.= "<p>Username :  <label style='color:#2156f3;font-weight: bold;'>".$mails."</label>";
        $body.= "<br>Password :  <label style='color:#2156f3;font-weight: bold;'>".$pass."</label></p>";
        $body.= "<p>We hope you enjoy our newest tool that allows you access to shared resources and training modules. If you have any questions, pleaseemail us at inspiredcommunity@exquisinethai.com!
        </p>";
        $body.= "<p>Thank you and with our very best regards, <br>COCA & Mango Tree Team</p>";
        $body.= "</div>";
		$body.= "</div>";
        // Email body content
		$mail->Body = $body;
        
        // Send email
        if(!$mail->send()){
            echo 'Message could not be sent.';
            echo 'Mailer Error: ' . $mail->ErrorInfo;
        }else{
            echo 'success';        
		}
    }
    
}