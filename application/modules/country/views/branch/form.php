<!-- ============================================================== -->
<div class="page-wrapper">
    <div class="container-fluid">

        <div class="row">
            <div class="col-md-12">
                <h2>Brand</h2>
                <hr><br>
                <?php echo form_open_multipart($Urlform); ?>
                <div class="form-group">
                    <div class="row">
                        <label for="title" class="col-md-2 control-label">Brand</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" id="title" name="title"
                                value="<?=isset($result->title) ? $result->title : '';?>" required>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label for="file" class="col-md-2 control-label"></label>
                        <div class="col-md-10">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new img-thumbnail" style="width: 200px; height: 150px;">
                                    <?php if (!empty($result->image)){ ?>
                                    <img src="<?=base_url('uploads/branch/'.$result->image);?>" alt="soso-solution">
                                    <?php }else{ ?>
                                    <img src="<?=base_url('image/picture.png');?>" alt="soso-solution">
                                    <?php } ?>
                                </div>
                                <div class="fileinput-preview fileinput-exists img-thumbnail"
                                    style="max-width: 200px; max-height: 150px;"></div>
                                <div>
                                    <span class="btn btn-outline-secondary btn-file">
                                        <span class="fileinput-new">เลือกรูปภาพ *</span>
                                        <span class="fileinput-exists">เปลี่ยน</span>
                                        <input type="file" name="image">
                                        <input type="hidden" name="image_hid"
                                            value="<?=!empty($result->image) ? $result->image : '';?>">
                                    </span>
                                    <a href="#" class="btn btn-outline-secondary fileinput-exists"
                                        data-dismiss="fileinput">ลบรูปภาพ</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="offset-md-2 col-md-10">
                            <input type="hidden" id="branchID" name="branchID"
                                value="<?=isset($result->branchID) ? $result->branchID : '';?>">
                            <button type="submit" class="btn btn-info">SAVE</button>
                        </div>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>

        <!-- ============================================================== -->
    </div>
</div>