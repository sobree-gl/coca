<!-- ============================================================== -->
<div class="page-wrapper">
    <div class="container-fluid">

        <div class="row">
            <div class="col-md-12">
                <h2>Access management</h2>
                <hr>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="listdata">
                    <div class="item-category">
                        <div class="sub active">Access management</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="text-right">
                    <a href="<?=$Urladd;?>">
                        <!-- <button type="button" class="btn btn-success"><i class="fas fa-plus"></i> Add</button> -->
                    </a>
                </div>

                <div class="listdata">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Organization</th>
                                <th class="text-center">Action</th>
                                <!-- <th class="text-center"></th> -->
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($result as $val) {
                            ?>
                            <tr>
                                <td>
                                    <div class="tr-col"><?=$val->title;?></div>
                                </td>
                                <td>
                                    <div class="tr-col text-center">
                                        <?php if ($val->title!='Corporate') { ?>
                                        <a href="<?=$Urlaction_group.'/'.$val->accessID;?>">
                                            <button type="button" class="btn btn-info"><i class="fas fa-plus"></i> Add role</button>
                                        </a>
                                        <?php }else{ ?>
                                        <a href="<?=$Urlaction.'/'.$val->accessID;?>">
                                            <button type="button" class="btn btn-success"><i class="fas fa-plus"></i> Add</button>
                                        </a>
                                        <?php } ?>
                                    </div>
                                </td>
                                <!-- <td>
                                    <div class="tr-col text-right">
                                        <a href="<?=$Urledit.'/'.$val->accessID;?>">
                                            <button type="button" class="btn btn-warning">Edit</button>
                                        </a>
                                        <a href="javascript:void(0);" click-delete="<?=$Urldelete.'/'.$val->accessID;?>">
                                            <button type="button" class="btn btn-danger">Delete</button>
                                        </a>
                                    </div>
                                </td> -->
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>

        <!-- ============================================================== -->
    </div>
</div>