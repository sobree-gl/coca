<!-- ============================================================== -->
<div class="page-wrapper">
    <div class="container-fluid">

        <div class="row">
            <div class="col-md-12">
                <h2>City</h2>
                <hr><br>
                <?php echo form_open_multipart($Urlform); ?>
                <div class="form-group">
                    <div class="row">
                        <label for="title" class="col-md-2 control-label">City</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" id="title" name="title"
                                value="<?=isset($result->title) ? $result->title : '';?>">
                        </div>
                    </div>
                </div>
                <!-- <div class="form-group">
                    <div class="row">
                        <label for="title" class="col-md-2 control-label">Franchisee</label>
                        <div class="col-md-10">
                            <select name="franchiseeID" placeholder="Select Franchisee.. " data-allow-clear="1">
                                <option></option>
                            <?php foreach ($result_franchisee as $val) { ?>
                                <?php if ($val->franchiseeID==$result->franchiseeID) { ?>
                                <option value="<?=isset($val->franchiseeID) ? $val->franchiseeID : '';?>" selected><?=isset($val->title) ? $val->title : '';?></option>
                                <?php } else { ?>
                                <option value="<?=isset($val->franchiseeID) ? $val->franchiseeID : '';?>"><?=isset($val->title) ? $val->title : '';?></option>
                                <?php } ?>
                            <?php } ?>
                            </select>
                        </div>
                    </div>
                </div> -->
                <div class="form-group">
                    <div class="row">
                        <div class="offset-md-2 col-md-10">
                            <input type="hidden" id="countryID" name="countryID"
                                value="<?=$this->session->URL_Zone['id'];?>">
                            <input type="hidden" id="zoneID" name="zoneID"
                                value="<?=isset($result->zoneID) ? $result->zoneID : '';?>">
                            <button type="submit" class="btn btn-info">SAVE</button>
                        </div>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>

        <!-- ============================================================== -->
    </div>
</div>