<!-- ============================================================== -->
<div class="page-wrapper">
    <div class="container-fluid">

        <div class="row">
            <div class="col-md-12">
                <h2>Country</h2>
                <hr><br>
                <?php echo form_open_multipart($Urlform); ?>
                    <div class="form-group">
                        <div class="row">
                            <label for="title" class="col-md-2 control-label">Country</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" id="title" name="title" value="<?=isset($result->title) ? $result->title : '';?>">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="offset-md-2 col-md-10">
                                <input type="hidden" id="countryID" name="countryID" value="<?=isset($result->countryID) ? $result->countryID : '';?>">
                                <button type="submit" class="btn btn-info">SAVE</button>
                            </div>
                        </div>
                    </div>
                <?php echo form_close(); ?>
            </div>
        </div>

        <!-- ============================================================== -->
    </div>
</div>