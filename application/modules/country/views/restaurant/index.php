<!-- ============================================================== -->
<div class="page-wrapper">
    <div class="container-fluid">

        <div class="row">
            <div class="col-md-12">
                <h2>Restaurant</h2>
                <hr>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="listdata">
                    <div class="item-category">
                        <div class="sub active">Restaurant</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <?php if ($this->session->sess_login['type']=='Corporate') { ?>
                <div class="text-right">
                    <a href="<?=$Urladd;?>">
                        <button type="button" class="btn btn-success"><i class="fas fa-plus"></i> Add</button>
                    </a>
                </div>
                <?php } ?>

                <div class="listdata">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Restaurant</th>
                                <!-- <th>detail</th> -->
                                <th class="text-center">Users</th>
                                <th class="text-center"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($result as $val) {
                            ?>
                            <tr>
                                <td>
                                    <div class="tr-col"><?=$val->title;?></div>
                                </td>
                                <!-- <td>
                                    <div class="tr-col"><?=$val->detail;?></div>
                                </td> -->
                                <td>
                                    <div class="tr-col text-center">
                                        <a href="<?=$Urlaction.'/'.$val->restaurantID;?>">
                                            <button type="button" class="btn btn-success"><i class="fas fa-plus"></i> Add users</button>
                                        </a>
                                    </div>
                                </td>
                                <td>
                                    <?php if ($this->session->sess_login['type']=='Corporate') { ?>
                                    <div class="tr-col text-right">
                                        <a href="<?=$Urledit.'/'.$val->restaurantID;?>">
                                            <button type="button" class="btn btn-warning">Edit</button>
                                        </a>
                                        <a href="javascript:void(0);" click-delete="<?=$Urldelete.'/'.$val->restaurantID;?>">
                                            <button type="button" class="btn btn-danger">Delete</button>
                                        </a>
                                    </div>
                                    <?php } ?>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>

        <!-- ============================================================== -->
    </div>
</div>