<!-- ============================================================== -->
<div class="page-wrapper">
    <div class="container-fluid">

        <div class="row">
            <div class="col-md-12">
                <h2>Restaurant</h2>
                <hr><br>
                <?php echo form_open_multipart($Urlform); ?>
                <div class="form-group">
                    <div class="row">
                        <label for="title" class="col-md-2 control-label">Restaurant</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" id="title" name="title"
                                value="<?=isset($result->title)?$result->title:'';?>" required>
                        </div>
                    </div>
                </div>
                <!-- <div class="form-group">
                    <div class="row">
                        <label for="title" class="col-md-2 control-label">detail</label>
                        <div class="col-md-10">
                            <textarea id="detail" name="detail"><?=isset($result->detail)?$result->detail:'';?></textarea>
                        </div>
                    </div>
                </div> -->
                <div class="form-group">
                    <div class="row">
                        <label for="title" class="col-md-2 control-label">Brand</label>
                        <div class="col-md-10">
                            <select class="form-control"name="branchID" placeholder="Select Brand.. ">
                                <option value="" hidden>-- Select Brand --</option>
                                <?php foreach ($branch as $val) { ?>
                                <?php if ($val->branchID==$result->branchID) { ?>
                                <option value="<?=isset($val->branchID)?$val->branchID:'';?>" selected><?=isset($val->title)?$val->title:'';?></option>
                                <?php } else { ?>
                                <option value="<?=isset($val->branchID)?$val->branchID:'';?>"><?=isset($val->title)?$val->title:'';?></option>
                                <?php } ?>
                            <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label for="title" class="col-md-2 control-label">Franchisee</label>
                        <div class="col-md-10">
                            <select class="form-control"name="franchiseeID" placeholder="Select Franchisee..">
                                <option value="" hidden>N/A</option>
                                <?php foreach ($franchisee as $val) { ?>
                                <?php if ($val->franchiseeID==$result->franchiseeID) { ?>
                                <option value="<?=isset($val->franchiseeID)?$val->franchiseeID:'';?>" selected><?=isset($val->title)?$val->title:'';?></option>
                                <?php } else { ?>
                                <option value="<?=isset($val->franchiseeID)?$val->franchiseeID:'';?>"><?=isset($val->title)?$val->title:'';?></option>
                                <?php } ?>
                            <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label for="title" class="col-md-2 control-label">Country</label>
                        <div class="col-md-10">
                            <select class="form-control" name="countryID"
                                    change-country="<?=site_url('home/country_ajax');?>">
                                <option value="" hidden>-- Select Country --</option>
                                <?php foreach ($country as $val) { ?>
                                <?php if ($val->countryID==$result->countryID) { ?>
                                <option value="<?=isset($val->countryID)?$val->countryID:'';?>" selected><?=isset($val->title)?$val->title:'';?></option>
                                <?php } else { ?>
                                <option value="<?=isset($val->countryID)?$val->countryID:'';?>"><?=isset($val->title)?$val->title:'';?></option>
                                <?php } ?>
                            <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label for="title" class="col-md-2 control-label">City</label>
                        <div class="col-md-10">
                            <div >
                                <select class="form-control" name="zoneID"
                                    change-zone="<?=site_url('home/restaurant_ajax');?>" id="respon-zone">
                                    <option value="" hidden>-- Select City --</option>
                                    <?php
                                    $this->db->where('countryID', $result->countryID);
                                    $this->db->from('country_zone');
                                    $country_zone = $this->db->get();
                                    foreach ($country_zone->result() as $value) {
                                    ?>
                                    <?php if ($value->zoneID==$result->zoneID) { ?>
                                    <option value="<?=isset($value->zoneID)?$value->zoneID:'';?>" selected><?=isset($value->title)?$value->title:'';?></option>
                                    <?php }else{ ?>
                                    <option value="<?=$value->zoneID;?>"><?=$value->title;?></option>
                                    <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="offset-md-2 col-md-10">
                            <input type="hidden" id="restaurantID" name="restaurantID"
                                value="<?=isset($result->restaurantID)?$result->restaurantID:'';?>">
                            <button type="submit" class="btn btn-info">SAVE</button>
                        </div>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>

        <!-- ============================================================== -->
    </div>
</div>