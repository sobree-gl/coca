<!-- ============================================================== -->
<div class="page-wrapper">
    <div class="container-fluid">

        <div class="row">
            <div class="col-md-12">
                <h2>User</h2>
                <hr><br>
                <?php echo form_open_multipart($Urlform); ?>
                <div class="form-group">
                    <div class="row">
                        <label for="title" class="col-md-2 control-label">Users Role</label>
                        <div class="col-md-10">
                            <select class="form-control" name="groupID" placeholder="Select Franchisee.. " required>
                                <option value="">-- Select Users Role --</option>
                                <?php foreach ($result_users_group as $val) { ?>
                                <?php if ($val->groupID==$result->groupID) { ?>
                                <option value="<?=isset($val->groupID) ? $val->groupID : '';?>" selected>
                                    <?=isset($val->title) ? $val->title : '';?></option>
                                <?php } else { ?>
                                <option value="<?=isset($val->groupID) ? $val->groupID : '';?>">
                                    <?=isset($val->title) ? $val->title : '';?></option>
                                <?php } ?>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label for="title" class="col-md-2 control-label">Image Profile</label>
                        <div class="col-md-10">
                        <input type="hidden" name="file_img_hid" value="<?=!empty($result->image) ? $result->image : '';?>">
                            <div class="custom-file" id="customFile" lang="es">
                                <input type="file" class="custom-file-input" name="file_img">
                                <label class="custom-file-label" for="exampleInputFile">
                                <?=!empty($result->image) ? $result->image : 'Select file...';?>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label for="must" class="col-md-2 control-label">Name title</label>
                        <div class="col-md-10">
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" value="Mr." name="title"
                                    checked>Mr.
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" value="Ms." name="title"
                                        <?php if ($result->title=='Ms.') {echo 'checked';} ?>>Ms.
                                </label>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label for="must" class="col-md-2 control-label">Name</label>
                        <div class="col-md-5">
                            <input type="text" class="form-control" id="firstname" name="firstname"
                                value="<?=isset($result->firstname) ? $result->firstname : '';?>" placeholder="First Name" required>
                        </div>
                        <div class="col-md-5">
                            <input type="text" class="form-control" id="lastname" name="lastname"
                                value="<?=isset($result->lastname) ? $result->lastname : '';?>" placeholder="Last Name" required>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label for="total" class="col-md-2 control-label">Position</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" id="position" name="position"
                                value="<?=isset($result->position) ? $result->position : '';?>">
                        </div>
                    </div>
                </div>
                <!-- <div class="form-group">
                    <div class="row">
                        <label for="title" class="col-md-2 control-label">username</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" id="username" name="username"
                                value="<?=isset($result->username) ? $result->username : '';?>">
                        </div>
                    </div>
                </div> -->
                <div class="form-group">
                    <div class="row">
                        <label for="total" class="col-md-2 control-label">Email</label>
                        <div class="col-md-10">
                            <input type="email" class="form-control" id="email" name="email"
                                value="<?=isset($result->email) ? $result->email : '';?>" required>
                        </div>
                    </div>
                </div>
                <?php if (!empty($result->usersID)) { ?>
                <div class="form-group">
                    <div class="row">
                        <label for="title" class="col-md-2 control-label">Password</label>
                        <div class="col-md-10">
                            <input type="password" class="form-control" id="password" name="password"
                                value="<?=isset($result->password) ? base64_decode($result->password) : '';?>" required>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <div class="form-group">
                    <div class="row">
                        <label for="must" class="col-md-2 control-label">Start Date</label>
                        <div class="col-md-10">
                            <input type="date" class="form-control" id="createDate" name="createDate"
                                value="<?=isset($result->createDate) ? $result->createDate : '';?>">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="offset-md-2 col-md-10">
                            <input type="hidden" id="typeID" name="typeID" value="<?=$this->session->Users['id'];?>">
                            <input type="hidden" id="type" name="type" value="<?=$this->session->Users['type'];?>">
                            <input type="hidden" id="usersID" name="usersID"
                                value="<?=isset($result->usersID) ? $result->usersID : '';?>">
                            <button type="submit" class="btn btn-info">SAVE</button>
                        </div>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>

        <!-- ============================================================== -->
    </div>
</div>