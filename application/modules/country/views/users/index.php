<!-- ============================================================== -->
<div class="page-wrapper">
    <div class="container-fluid">

        <div class="row">
            <div class="col-md-12">
                <h2>User</h2>
                <hr>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="listdata">
                    <div class="item-category">
                        <a href="<?=$this->session->back_users['link'];?>">
                            <div class="sub"><?=$this->session->Users['type'];?></div>
                        </a>
                        <div class="sub active">User</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <?php
                $users_group = modules::run('userspermis/users_group');
                if ($users_group->title=='Restaurant Manager'||$users_group->title=='Admin'||$this->session->sess_login['type']=='Corporate') {
                ?>
                <div class="text-right">
                    <a href="<?=$Urladd;?>">
                        <button type="button" class="btn btn-success"><i class="fas fa-plus"></i> Add</button>
                    </a>
                </div>
                <?php } ?>

                <div class="listdata">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <!-- <th>Username</th> -->
                            <th>Role</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($result as $val) {
                        ?>
                        <tr>
                            <td>
                                <div class="tr-col"><?=$val->firstname;?></div>
                            </td>
                            <td>
                                <div class="tr-col"><?=$val->email;?></div>
                            </td>
                            <!-- <td>
                                <div class="tr-col"><?=$val->username;?></div>
                            </td> -->
                            <td>
                                <?php $val_group = $this->db->get_where('users_group', array('groupID' => $val->groupID))->row();?>
                                <div class="tr-col"><?=$val_group->title;?></div>
                            </td>
                            <td>
                                <?php 
                                if ($users_group->title=='Restaurant Manager'||$users_group->title=='Admin'||$this->session->sess_login['type']=='Corporate') {
                                ?>
                                <div class="tr-col text-right">
                                    <a href="<?=$Urledit.'/'.$val->usersID;?>">
                                        <button type="button" class="btn btn-warning">Edit</button>
                                    </a>
                                    <!-- <a href="javascript:void(0);"
                                        click-delete="<?=$Urldelete.'/'.$val->usersID;?>">
                                        <button type="button" class="btn btn-danger">Delete</button>
                                    </a> -->
                                    <?php if ($val->deactivate==1) { ?>
                                    <a href="javascript:void(0);"
                                        click-deactivate="<?=$url_deactivate.'/'.$val->deactivate.'/'.$val->usersID;?>">
                                        <button type="button" class="btn btn-success">Deactivate</button>
                                        <br><span style="color: rgb(81, 81, 81);font-size: 13px;margin-right: 9px;">Activate</span>
                                    </a>
                                    <?php }else{ ?>
                                        <a href="javascript:void(0);"
                                        click-deactivate="<?=$url_deactivate.'/'.$val->deactivate.'/'.$val->usersID;?>">
                                        <button type="button" class="btn btn-danger">Activate</button>
                                        <br><span style="color: rgb(81, 81, 81);font-size: 13px;margin-right: 9px;">Deactivate</span>
                                    </a>
                                    <?php } ?>
                                </div>
                                <?php } ?>
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
                </div>

            </div>
        </div>

        <!-- ============================================================== -->
    </div>
</div>