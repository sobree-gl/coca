<div class="modal-header">
    <h5 class="modal-title">User Permission</h5>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
</div>
<div class="modal-body">

    <div class="container">
        <div class="row">
            <div class="col-12">
                <form action="<?=site_url('country/access_group/users_permission_save');?>" method="post" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-6">
                            <h3><b>Menu</b></h3>
                        </div>
                        <div class="col-6">
                            <h3><b>Permission</b></h3>
                        </div>
                    </div>
                    <hr style="margin-top: 0;">
                    <?php 
                    $menu = $this->db->get('menu');
                    foreach ($menu->result() as $key => $val) {
                        // menu_sub
                        $this->db->where('menuID', $val->menuID);
                        $menu_sub = $this->db->get('menu_sub');
                    ?>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group" style="margin: 0;">
                                <div class="list-menu">
                                    <h5 style="margin: 0;"><b><?=$val->label;?></b></h5>
                                    <?php foreach ($menu_sub->result() as $key => $val_sub) { ?>
                                    <p style="margin: 0;"> - <?=$val_sub->label;?></p>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div style="height: 18px;">
                                <!-- <div class="form-check-inline">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-check-input" name="read[]" value="<?=$val->menuID;?>">READ
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-check-input" name="add[]" value="<?=$val->menuID;?>">ADD
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-check-input" name="edit[]" value="<?=$val->menuID;?>">EDIT
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-check-input" name="delete[]" value="<?=$val->menuID;?>">DELETE
                                    </label>
                                </div> -->
                            </div>
                            <?php foreach ($menu_sub->result() as $key => $val_sub) {
                                $menuID = $val->menuID.'|'.$val_sub->subID;
                            ?>
                            <div>
                                <?php 
                                // users_permission_read
                                $this->db->where('subID', $val_sub->subID);
                                $this->db->where('groupID', $groupID);
                                $per_read = $this->db->get('users_permission_read')->row();
                                // users_permission_read
                                $this->db->where('subID', $val_sub->subID);
                                $this->db->where('groupID', $groupID);
                                $per_add = $this->db->get('users_permission_add')->row();
                                // users_permission_read
                                $this->db->where('subID', $val_sub->subID);
                                $this->db->where('groupID', $groupID);
                                $per_edit = $this->db->get('users_permission_edit')->row();
                                // users_permission_read
                                $this->db->where('subID', $val_sub->subID);
                                $this->db->where('groupID', $groupID);
                                $per_delete = $this->db->get('users_permission_delete')->row();
                                ?>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-check-input" name="read[]" value="<?=$menuID;?>" <?php if ($per_read->subID==$val_sub->subID) { echo "checked";} ?>>READ
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-check-input" name="add[]" value="<?=$menuID;?>" <?php if ($per_add->subID==$val_sub->subID) { echo "checked";} ?>>ADD
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-check-input" name="edit[]" value="<?=$menuID;?>" <?php if ($per_edit->subID==$val_sub->subID) { echo "checked";} ?>>EDIT
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-check-input" name="delete[]" value="<?=$menuID;?>" <?php if ($per_delete->subID==$val_sub->subID) { echo "checked";} ?>>DELETE
                                    </label>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                        <div class="col-12">
                            <hr style="margin: 10px 0;">
                        </div>
                    </div>
                    <?php } ?>
                    <input type="hidden" id="groupID" name="groupID" value="<?=isset($groupID) ? $groupID : '';?>">
                    <button type="submit" class="btn btn-primary">SAVE <i class="far fa-save"></i></button>
                </form>
            </div>
        </div>
    </div>

</div>
<div class="modal-footer">
    <button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">CLOSE <i
            class="far fa-times-circle"></i></button>
</div>