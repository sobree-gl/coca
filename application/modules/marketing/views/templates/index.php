<!-- ============================================================== -->
<div class="page-wrapper">
    <div class="container-fluid">

        <div class="row">
            <div class="col-md-12">
                <h2>Marketing</h2>
                <hr>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="listdata">
                    <div class="item-category">
                        <a href="">
                            <div class="sub active">Marketing Templates</div>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="listdata">

            <table class="table">
                <thead>
                    <tr>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php for ($i=0; $i < 8; $i++) { ?>
                    <tr>
                        <td>
                            <div class="tr-col">
                                <p class="head">Marketing Toolkit</p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi lacus lacus, congue ut
                                    finibus semper, ornare ac orci.</p>
                            </div>
                        </td>
                        <td>
                            <div class="tr-col text-right">
                                <a href="<?=base_url('uploads/operations/manuals/'.$value['files']);?>" target="_blank">
                                    <button type="button" class="btn btn-success">Download</button>
                                </a>
                            </div>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>

        </div>

        <!-- ============================================================== -->
    </div>
</div>