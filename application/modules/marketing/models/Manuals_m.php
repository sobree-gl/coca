<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Manuals_m extends CI_Model {


	public function __construct() {
		parent::__construct(); 
	}

	// Fetch records
	public function pagination_getData($rowno,$rowperpage) {
			
		$this->db->select('*');
		$this->db->from('operations_manuals');
		$this->db->where('category_id', $this->input->get('whreID'));
        $this->db->limit($rowperpage, $rowno);  
		$query = $this->db->get();
       	
		return $query->result_array();
	}

	// Select total records
    public function pagination_getCount() {

    	$this->db->select('count(*) as allcount');
      	$this->db->from('operations_manuals');
		$this->db->where('category_id', $this->input->get('whreID'));
		$query = $this->db->get();
      	$result = $query->result_array();
      
      	return $result[0]['allcount'];
    }

}