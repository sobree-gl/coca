<!-- sweetalert2 -->
<script type="text/javascript" src="<?=base_url('plugin/sweetalert2/sweetalert2.min.js');?>"></script>
<script>
    $('a[click-delete]').click(function (e) {
        alert( $(this).attr('click-delete') );
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                window.location.replace("<?=site_url('corporate/training/orientation');?>");
            }
        })
    });
</script>