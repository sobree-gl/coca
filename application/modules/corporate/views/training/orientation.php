<!-- ============================================================== -->
<div class="page-wrapper">
    <div class="container-fluid">

        <div class="row">
            <div class="col-md-12">
                <h2>Corporate</h2>
                <hr>
                <div class="">
                    <a href="<?=site_url('corporate/training/orientation_form');?>">
                        <button type="button" class="btn btn-success"><i class="fas fa-plus"></i> Add</button>
                    </a>
                </div>
                <div class="listdata">
                    <div class="item-head">
                        <div>TOPIC</div>
                        <div>CATEGORY</div>
                        <div>TOTAL QUESTION</div>
                        <div>MARKS</div>
                        <div class="text-center"></div>
                        <div class="text-center"></div>
                    </div>
                </div>
                <div class="listdata">
                    <?php
                    foreach ($get_all as $val) {
                    ?>
                    <div class="item-data">
                        <div class="blog"><?=$val->title;?></div>
                        <div class="blog"><?=$val->category;?></div>
                        <div class="blog"><?=$val->total;?></div>
                        <div class="blog"><?=$val->must;?></div>
                        <div class="blog">
                            <a href="<?=site_url('corporate/training/question/'.base64_encode($val->id));?>">
                                <button type="button" class="btn btn-info"><i class="fas fa-plus"></i> QUESTION</button>
                            </a>
                        </div>
                        <div class="blog text-center">
                            <a href="<?=site_url('corporate/training/orientation_form/'.base64_encode($val->id));?>">
                                <button type="button" class="btn btn-warning">Edit</button>
                            </a>
                            <a href="javascript:void(0);" click-delete="<?=site_url('corporate/training/orientation_delete/'.base64_encode($val->id));?>">
                                <button type="button" class="btn btn-danger">Delete</button>
                            </a>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>

        <!-- ============================================================== -->
    </div>
</div>