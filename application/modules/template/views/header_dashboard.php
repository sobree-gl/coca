<!-- ============================================================== -->
<!-- Topbar header - style you can find in pages.scss -->
<!-- ============================================================== -->
<header class="topbar">
    <nav class="navbar top-navbar navbar-expand-md navbar-light">
        <!-- ============================================================== -->
        <!-- Logo -->
        <!-- ============================================================== -->
        <div class="navbar-header" style="text-align: center;padding: 0;">
            <a class="navbar-brand" href="<?=site_url('');?>">
                <img src="<?=base_url('image/logo/logo.png');?>" alt="">
            </a>
        </div>
        <!-- ============================================================== -->
        <!-- End Logo -->
        <!-- ============================================================== -->
        <div class="navbar-collapse">
            <?php
            $branch = modules::run('userspermis/branch');
            $branchs =  $this->db->get_where('branch', array('branchID' => $branch->branchID))->row();
            ?>
            <?php if ($this->session->sess_login['type']=='Restaurant') { ?>
            <img src="<?=base_url('uploads/branch/'.$branchs->image);?>" alt="" style="height: 35px;padding: 5px;">
            <?php }else{ ?>
            <?php
            $branch2 = $this->db->get('branch');
            foreach ($branch2->result() as $branchs2) {
            ?>
            <img src="<?=base_url('uploads/branch/'.$branchs2->image);?>" alt="" style="height: 35px;padding: 5px;">
            <?php } ?>
            <?php } ?>
            <!-- ============================================================== -->
            <!-- toggle and nav items -->
            <!-- ============================================================== -->
            <ul class="navbar-nav mr-auto">
            </ul>
            <!-- ============================================================== -->
            <!-- User profile and search -->
            <!-- ============================================================== -->
            <ul class="navbar-nav my-lg-0">
                <!-- ============================================================== -->
                <!-- Profile -->
                <!-- ============================================================== -->
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle waves-effect waves-dark" href="javascript:void(0);" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                        <?php
                        $get_users = modules::run('userspermis/get_users');
                        if ($get_users->image) {
                        ?>
                        <img src="<?=base_url('/uploads/users/'.$get_users->image);?>" alt="user" class="profile-pic" />
                        <?php }else{ ?>
                        <img src="<?=base_url('image/user.png');?>" alt="user" class="profile-pic" />
                        <?php } ?>
                        <div class="name">
                            <?php
                            $users_group = modules::run('userspermis/users_group');
                            ?>
                            <p>Welcome, <?=$this->session->sess_login['firstname'];?> <i class="fas fa-angle-down"></i></p>
                            <span><?=$users_group->title;?></span>
                        </div>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right animated flipInY">
                        <ul class="dropdown-user">
                            <li>
                                <div class="dw-user-box">
                                    <div class="u-img">
                                    <?php if ($get_users->image) { ?>
                                        <img src="<?=base_url('/uploads/users/'.$get_users->image);?>" alt="user"/>
                                    <?php }else{ ?>
                                        <img src="<?=base_url('image/user.png');?>" alt="user">
                                    <?php } ?>
                                    </div>
                                    <div class="u-text">
                                        <h4><?=$this->session->sess_login['firstname'];?></h4>
                                        <p class="text-muted"><?=$this->session->sess_login['email'];?></p>
                                    </div>
                                </div>
                            </li>
                            <li role="separator" class="divider"></li>
                            <li><a href="<?=site_url('setting/profile/form/'.$this->session->sess_login['usersID']);?>"><i class="fas fa-user-cog"></i> Account Setting</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="<?=site_url('home/logout');?>"><i class="fa fa-power-off"></i> Logout</a></li>
                        </ul>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
</header>
<!-- ============================================================== -->
<!-- End Topbar header -->
<!-- ============================================================== -->
