<?php if ($this->session->userdata('sess_login')=='') { redirect(base_url('home/login'), 'refresh'); } ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php echo $seo;?>
	<!-- <link rel="shortcut icon" href="<?=base_url('image/logo/favicon.ico');?>" type="image/x-icon"> -->
	<?php $this->load->view('template/function/default_stylesheet');?>
	<?php
	if(!empty($function)){
		$length1 = count($function);
		for ($x = 0; $x < $length1; $x++) {
			$this->load->view($function[$x].'/function/stylesheet');
		}
	}
	?>
</head>
<body class="fix-header card-no-border fix-sidebar">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Admin Pro</p>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">

	<?php $this->load->view('template/'.$header);?>
	<?php $this->load->view($content);?>
	<?php $this->load->view('template/'.$footer);?>

	<?php $this->load->view('template/function/default_script');?>
	<?php
	if(!empty($function)){
		$length2 = count($function);
		for ($x = 0; $x < $length2; $x++) {
			$this->load->view($function[$x].'/function/script');
		}
	}
	?>
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->

</body>
</html>