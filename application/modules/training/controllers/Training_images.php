<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Training_images extends MX_Controller {

	public function __construct()
	{
        parent::__construct();
		// Load model
        $this->load->model('DBrecord');

        $this->id = end($this->uri->segment_array());
        $this->table = 'training_images';
	}

    private function seo()
	{
		$title          = "Brandoverview / Images";
		$robots         = "noindex,nofollow";
		$description    = "titlewebtitleweb";
		$keywords       = "titleweb,titleweb";
		$meta  			= '<TITLE>'.$title.'</TITLE>';
		$meta 		   .= '<meta name="robots" content="'.$robots.'"/>';
		$meta		   .= '<meta name="description" content="'.$description.'"/>';
        $meta 		   .= '<meta name="keywords" content="'.$keywords.'"/>';
        $meta 		   .= '<meta property="og:url" content="'.site_url().'" />';
        $meta 		   .= '<meta property="og:type" content="website" />';
        $meta 		   .= '<meta property="og:title" content="'.$title.'" />';
        $meta 		   .= '<meta property="og:description" content="'.$description.'" />';
        $meta 		   .= '<meta property="og:image" content="'.base_url('image/logo/logo.png').'" />';
		return $meta;
    }
    
    private function SiteURL($SiteURL)
	{
		$SiteURL = site_url('training/training_images/'.$SiteURL);
        return $SiteURL;
	}
    
    private function thisURL()
	{
		$sess_data = array(
            'id' => $this->id,
            'link' => current_url()
        );
        $this->session->set_userdata('training_images',$sess_data);
    }

	public function index()
	{
        $this->thisURL();
        $data = array(
            'seo'     => $this->seo(),
            'menu'    => 'training',
            'header'  => 'header',
            'content' => 'training_images/form',
            'footer'  => 'footer',
            'function'=>  array('training'),
        );
        // DBrecord //
        $DBrecord['id'] = array('imagesID' => $this->id);
        $DBrecord['table'] = $this->table;
        $data['result'] = $this->DBrecord->get_result($DBrecord);
        // DBrecord //
        $data['url_form'] = $this->SiteURL('multiple_upload');
        $data['url_delete'] = $this->SiteURL('delete');
        $this->load->view('template/body', $data);
    }
    
    public function multiple_upload()
    {
        if(isset($_FILES['filename']) && !empty($_FILES['filename']['name'])) {
            $filesCount = count($_FILES['filename']['name']);
            for($i = 0; $i < $filesCount; $i++){
                $_FILES['file']['name'] = $_FILES['filename']['name'][$i];
                $_FILES['file']['type'] = $_FILES['filename']['type'][$i];
                $_FILES['file']['tmp_name'] = $_FILES['filename']['tmp_name'][$i];
                $_FILES['file']['error'] = $_FILES['filename']['error'][$i];
                $_FILES['file']['size'] = $_FILES['filename']['size'][$i];
                $config['upload_path'] = './uploads/training/brandoverview/'; 
                $config['allowed_types'] = 'jpg|jpeg|png|JPEG|JPG|PNG';
                $config['encrypt_name']  = TRUE;
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if($this->upload->do_upload('file')){
                    $fileData = $this->upload->data();

                    $this->load->library('image_lib');
                    $configer =  array(
                        'image_library'   => 'gd2',
                        'source_image'    =>  $fileData['full_path'],
                        'maintain_ratio'  =>  TRUE,
                        'width'           =>  900,
                        'height'          =>  900,
                    );
                    $this->image_lib->clear();
                    $this->image_lib->initialize($configer);
                    $this->image_lib->resize();

                    $uploadData[$i]['files'] = $fileData['file_name'];
                    $uploadData[$i]['groupID'] = $this->input->post('groupID');
                    $uploadData[$i]['createDate'] = date('Y-m-d H:i:s');
                }
            }
            if(!empty($uploadData)){
                $this->db->insert_batch($this->table ,$uploadData);
            }   
        }
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function delete()
	{
        // DBrecord //
        $DBrecord['id'] = array('imagesID' =>  end($this->uri->segment_array()));
        $DBrecord['table'] = $this->table;

        $result = $this->DBrecord->get_first($DBrecord);
        if ($result->files) {
            unlink('./uploads/training/brandoverview/'.$result->files);
        }

        $this->DBrecord->delete($DBrecord);
        // DBrecord //
        redirect($_SERVER['HTTP_REFERER']);
	}
    
}