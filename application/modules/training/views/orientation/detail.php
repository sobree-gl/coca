<!-- Start Page Content -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <div class="container-fluid">

        <div class="row">
            <div class="col-md-12">
                <h2>Orientation</h2>
                <hr>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="listdata">
                    <div class="item-category">
                        <a href="<?=site_url('training/orientation');?>">
                            <div class="sub active">Orientation</div>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <?php
        $this->db->where('id', base64_decode($this->uri->segment(4)));
        $training_group = $this->db->get('training_group');
        foreach ($training_group->result() as $val) {
        ?>
        <div class="row mt-1">
            <div class="col-md-7">
                <video id="training" poster="<?=base_url('image/logo/logo.jpg');?>" controls style="width: 100%;background: rgb(255, 255, 255);border: 1px solid rgb(241, 241, 241);">
                    <source src="<?=base_url('uploads/training/orientation/'.$val->video);?>" />
                </video>
            </div>
            <div class="col-md-5">
                <h3><?=$val->title;?></h3>
                <hr>
                <?=$val->detail;?>
                <div class="text-center mt-4">
                    <a href="<?=site_url('training/orientation/choice/'.base64_encode($val->id));?>" id="btn_start">
                        <button type="button" class="btn btn-secondary WidthFull" style="font-size: 25px;" disabled>Question <i
                                class="fas fa-tasks"></i></button>
                    </a>
                </div>
            </div>
        </div>
        <?php } ?>

        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
</div>