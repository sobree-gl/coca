<!-- ============================================================== -->
<div class="page-wrapper">
    <div class="container-fluid">

        <div class="row">
            <div class="col-md-12">
                <h2>Orientation</h2>
                <hr><br>
                <?php echo form_open_multipart($Urlform); ?>
                <div class="form-group">
                    <div class="row">
                        <label for="title" class="col-md-2 control-label">Sort</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" id="sort" name="sort"
                                value="<?=isset($result->sort) ? $result->sort : '';?>" required>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label for="title" class="col-md-2 control-label">Title</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" id="title" name="title"
                                value="<?=isset($result->title) ? $result->title : '';?>" required>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label for="title" class="col-md-2 control-label">Video</label>
                        <div class="col-md-10">
                        <input type="hidden" name="file_doc_hid" value="<?=!empty($result->video) ? $result->video : '';?>">
                            <div class="custom-file" id="customFile" lang="es">
                                <input type="file" class="custom-file-input" name="file_doc">
                                <label class="custom-file-label" for="exampleInputFile">
                                <?=!empty($result->video) ? $result->video : 'Select file...';?>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label for="title" class="col-md-2 control-label">Image</label>
                        <div class="col-md-10">
                        <input type="hidden" name="file_img_hid" value="<?=!empty($result->image) ? $result->image : '';?>">
                            <div class="custom-file" id="customFile" lang="es">
                                <input type="file" class="custom-file-input" name="file_img">
                                <label class="custom-file-label" for="exampleInputFile">
                                <?=!empty($result->image) ? $result->image : 'Select file...';?>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label for="title" class="col-md-2 control-label">Detail</label>
                        <div class="col-md-10">
                            <textarea id="detail"
                                name="detail"><?=isset($result->detail)?$result->detail:'';?></textarea>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label for="title" class="col-md-2 control-label">Total</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" id="total" name="total"
                                value="<?=isset($result->total) ? $result->total : '';?>" required>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label for="title" class="col-md-2 control-label">Must</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" id="must" name="must"
                                value="<?=isset($result->must) ? $result->must : '';?>" required>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="offset-md-2 col-md-10">
                            <input type="hidden" id="id" name="id"
                                value="<?=isset($result->id) ? $result->id : '';?>">
                            <button type="submit" class="btn btn-info">SAVE</button>
                        </div>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>

        <!-- ============================================================== -->
    </div>
</div>