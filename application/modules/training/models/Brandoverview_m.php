<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Brandoverview_m extends CI_Model {


	public function __construct() {
		parent::__construct(); 
	}

	// Fetch records
	public function pagination_getData($rowno,$rowperpage) {
			
		$this->db->select('*');
		$this->db->from('training_group');
		$this->db->like('groups', 'Brand Overview');
		$this->db->order_by('id', 'ASC');
        $this->db->limit($rowperpage, $rowno);  
		$query = $this->db->get();
       	
		return $query->result_array();
	}

	// Select total records
    public function pagination_getCount() {

    	$this->db->select('count(*) as allcount');
      	$this->db->from('training_group');
		$this->db->like('groups', 'Brand Overview');
		$this->db->order_by('id', 'ASC');
        $query = $this->db->get();
      	$result = $query->result_array();
      
      	return $result[0]['allcount'];
    }

}