<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Userspermis extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('DBrecord');

	}

	// $userspermis = modules::run('userspermis/franchisee')
	public function franchisee()
	{
		// DBrecord //
		$DBrecord['id'] = array('franchiseeID' => $this->session->sess_login['typeID']);
		$DBrecord['table'] = 'franchisee';
		$data['result'] = $this->DBrecord->get_result($DBrecord);
		// DBrecord //
		return $data;
	}

	public function restaurant()
	{
		// DBrecord //
		$DBrecord['id'] = array('restaurantID' => $this->session->sess_login['typeID']);
		$DBrecord['table'] = 'restaurant';
		$data['result'] = $this->DBrecord->get_result($DBrecord);
		// DBrecord //
		return $data;
	}

	public function get_users()
	{
		// DBrecord //
		$DBrecord['id'] = array('usersID' => $this->session->sess_login['usersID']);
		$DBrecord['table'] = 'users';
		$result = $this->DBrecord->get_first($DBrecord);
		// DBrecord //
		return $result;
	}

	public function users_group()
	{
		// DBrecord //
		$DBrecord['id'] = array('groupID' => $this->session->sess_login['groupID']);
		$DBrecord['table'] = 'users_group';
		$result = $this->DBrecord->get_first($DBrecord);
		// DBrecord //
		return $result;
	}

	public function branch()
	{
		// DBrecord //
		$DBrecord['id'] = array('restaurantID' => $this->session->sess_login['typeID']);
		$DBrecord['table'] = 'restaurant';
		$result = $this->DBrecord->get_first($DBrecord);
		// DBrecord //
		return $result;
	}

	public function training_answer($group_id=null) {
		// เชื่อมต่อ training
		$CI =& get_instance();
		$CI->db->from('training a'); 
		$CI->db->where('a.training_group_id', $group_id);
		$training = $CI->db->get(); 
		foreach ($training->result() as $val_training) {
			// เชื่อมต่อ training_answer
			$CI->db->from('training_answer a'); 
			$CI->db->where('a.user_id', $CI->session->sess_login['usersID']);
			$CI->db->where('a.training_id', $val_training->id);
			$training_choice = $CI->db->get(); 
			$val_training_choice = $training_choice->row(); 
			// $score = array();
			if ($training_choice->num_rows()>0) {
				$score[] = $val_training_choice->score;
			}
		}
		// รวมคะแนน
		if ($training->num_rows()>0&&$training_choice->num_rows()>0) {
			$array_sum = array_sum($score);
			return (int)$array_sum;
		}else{
			return null;
		}
	}

}