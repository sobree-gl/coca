<!DOCTYPE html>
<html lang="en">

<head>
    <title>Login system</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="icon" type="image/png" href="<?=base_url('plugin/login/images/icons/favicon.ico');?>" />

    <link rel="stylesheet" type="text/css" href="<?=base_url('plugin/login/vendor/bootstrap/css/bootstrap.min.css');?>">

    <link rel="stylesheet" type="text/css" href="<?=base_url('plugin/login/fonts/font-awesome-4.7.0/css/font-awesome.min.css');?>">

    <link rel="stylesheet" type="text/css" href="<?=base_url('plugin/login/fonts/iconic/css/material-design-iconic-font.min.css');?>">

    <link rel="stylesheet" type="text/css" href="<?=base_url('plugin/login/vendor/animate/animate.css');?>">

    <link rel="stylesheet" type="text/css" href="<?=base_url('plugin/login/vendor/css-hamburgers/hamburgers.min.css');?>">

    <link rel="stylesheet" type="text/css" href="<?=base_url('plugin/login/vendor/animsition/css/animsition.min.css');?>">

    <link rel="stylesheet" type="text/css" href="<?=base_url('plugin/login/vendor/select2/select2.min.css');?>">

    <link rel="stylesheet" type="text/css" href="<?=base_url('plugin/login/vendor/daterangepicker/daterangepicker.css');?>">

    <link rel="stylesheet" type="text/css" href="<?=base_url('plugin/login/css/util.css');?>">
    <link rel="stylesheet" type="text/css" href="<?=base_url('plugin/login/css/main.css');?>">

</head>

<body>
    <div class="limiter">
        <div class="container-login100" style="background-image: url(<?=base_url('image/bg/bg-login.jpg');?>);">
            <div class="wrap-login100">
                <form class="login100-form validate-form" action="<?=site_url('home/checklogin');?>" method="post">
                    <span class="login100-form-logo">
                        <i class="zmdi zmdi-key"></i>
                    </span>
                    <span class="login100-form-title p-b-34 p-t-27">
                        Log in
                    </span>
                    <div class="wrap-input100 validate-input" data-validate="Enter email">
                        <input class="input100" type="text" name="username" placeholder="email">
                        <span class="focus-input100" data-placeholder="&#xf207;"></span>
                    </div>
                    <div class="wrap-input100 validate-input" data-validate="Enter password">
                        <input class="input100" type="password" name="password" placeholder="Password">
                        <span class="focus-input100" data-placeholder="&#xf191;"></span>
                    </div>
                    <div class="contact100-form-checkbox">
                        <input class="input-checkbox100" id="ckb1" type="checkbox" name="remember-me">
                        <label class="label-checkbox100" for="ckb1">
                            Remember me
                        </label>
                    </div>
                    <div class="container-login100-form-btn">
                        <button class="login100-form-btn">
                            Login
                        </button>
                    </div>
                    <!-- <div class="text-center p-t-90">
                        <a class="txt1" href="<?=base_url('plugin/login/#');?>">
                            Forgot Password?
                        </a>
                    </div> -->
                </form>
            </div>
        </div>
    </div>
    <div id="dropDownSelect1"></div>

    <script src="<?=base_url('plugin/login/vendor/jquery/jquery-3.2.1.min.js');?>" type="362b701f4558bd9a1f35e938-text/javascript"></script>

    <script src="<?=base_url('plugin/login/vendor/animsition/js/animsition.min.js');?>" type="362b701f4558bd9a1f35e938-text/javascript"></script>

    <script src="<?=base_url('plugin/login/vendor/bootstrap/js/popper.js');?>" type="362b701f4558bd9a1f35e938-text/javascript"></script>
    <script src="<?=base_url('plugin/login/vendor/bootstrap/js/bootstrap.min.js');?>" type="362b701f4558bd9a1f35e938-text/javascript"></script>

    <script src="<?=base_url('plugin/login/vendor/select2/select2.min.js');?>" type="362b701f4558bd9a1f35e938-text/javascript"></script>

    <script src="<?=base_url('plugin/login/vendor/daterangepicker/moment.min.js');?>" type="362b701f4558bd9a1f35e938-text/javascript"></script>
    <script src="<?=base_url('plugin/login/vendor/daterangepicker/daterangepicker.js');?>" type="362b701f4558bd9a1f35e938-text/javascript"></script>

    <script src="<?=base_url('plugin/login/vendor/countdowntime/countdowntime.js');?>" type="362b701f4558bd9a1f35e938-text/javascript"></script>

    <script src="<?=base_url('plugin/login/js/main.js');?>" type="362b701f4558bd9a1f35e938-text/javascript"></script>

    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"
        type="362b701f4558bd9a1f35e938-text/javascript"></script>
    <script type="362b701f4558bd9a1f35e938-text/javascript">
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-23581568-13');
    </script>
    <script src="https://ajax.cloudflare.com/cdn-cgi/scripts/7089c43e/cloudflare-static/rocket-loader.min.js');?>"
        data-cf-settings="362b701f4558bd9a1f35e938-|49" defer=""></script>
</body>

</html>