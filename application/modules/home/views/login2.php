<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Login system</title>
  <!-- CSS -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp"
    crossorigin="anonymous">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?=base_url('class/css/app.css');?>">
  <link rel="stylesheet" href="<?=base_url('class/css/login.css');?>">

</head>

<body id="gradient">

  <!-- Top content -->
  <!-- <div class="top-content">

    <div class="inner-bg">
      <div class="container">
        <div class="row">
          <div class="col-sm-6 offset-sm-3 form-box">
            <div class="form-top">
              <div class="form-top-left">
                <h3>ADMIN login</h3>
                <p>Enter your username and password to log on :</p>
              </div>
              <div class="form-top-right">
                <i class="fa fa-lock"></i>
              </div>
            </div>
            <div class="form-bottom">
              <form role="form" action="<?=base_url();?>admin/adminlogin/?type=login" method="post" class="login-form">
                <div class="form-group">
                  <label class="sr-only" for="form-username">Username</label>
                  <input type="text" name="admin_user" placeholder="Username..." class="form-username form-control" id="form-username">
                </div>
                <div class="form-group">
                  <label class="sr-only" for="form-password">Password</label>
                  <input type="password" name="admin_pass" placeholder="Password..." class="form-password form-control" id="form-password">
                </div>
                <button type="submit" class="btn">SIGN IN</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div> -->

  <section>
    <div class="container">
      <div class="login-form">
        <div class="text-center">
            <img src="<?=base_url('image/logo/logo.png');?>" alt="" style="height: 100px;">
        </div>
        <br>
        <h1>lOG IN</h1>
        <form role="form" action="<?=site_url('home/checklogin');?>" method="post">
          <input type="text" name="username" placeholder="Enter Email">
          <input type="password" name="password" placeholder="Enter Password">
          <input type="submit" name="" value="Login">
        </form>

        <div class="mb-3" style="width: 100%;font-size: 15px;">
          <a href="javascript:;" data-toggle="modal" data-target="#myModal" style="color: rgb(239, 143, 39);text-transform: inherit;">
            <b><u>Forget password <i class="fas fa-key"></i></u></b>
          </a>
        </div>

        <?php
        $branch = $this->db->get('branch');
        foreach ($branch->result() as $key => $val) {
        ?>
        <div class="col <?php if ($key==0){echo '';}?>">
            <a href="<?=site_url('resources/images/detail/'.$val->branchID);?>">
                <div class="item-image">
                    <img src="<?=base_url('uploads/branch/'.$val->image);?>" alt="" class="wfull">
                </div>
            </a>
        </div>
        <?php } ?>
        <!-- <a href="#">Forget Password<a> -->
      </div>
    </div>
  </section>

  <!-- The Modal -->
  <div class="modal fade" id="myModal">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Forget password <i class="fas fa-key"></i></h4>
          <button type="button" class="close" data-dismiss="modal">×</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          <?php echo form_open_multipart(site_url('home/forget')); ?>
            <div class="form-group">
                <div class="row">
                    <label for="total" class="col-md-12 control-label">Enter your email</label>
                    <div class="col-md-12">
                        <input type="email" class="form-control" id="email" name="email" placeholder="Enter your email.." required>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-md-12 text-right">
                        <button type="submit" class="btn btn-success">Send</button>
                    </div>
                </div>
            </div>
          <?php echo form_close(); ?>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
        
      </div>
    </div>
  </div>


  <!-- jquery -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <!-- bootstrap -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</body>

</html>