<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('DBrecord');
        $this->load->helper('string');

	}

    private function seo()
	{
		$title          = "Home";
		$robots         = "noindex,nofollow";
		$description    = "titlewebtitleweb";
		$keywords       = "titleweb,titleweb";
		$meta  			= '<TITLE>'.$title.'</TITLE>';
		$meta 		   .= '<meta name="robots" content="'.$robots.'"/>';
		$meta		   .= '<meta name="description" content="'.$description.'"/>';
        $meta 		   .= '<meta name="keywords" content="'.$keywords.'"/>';
        $meta 		   .= '<meta property="og:url" content="'.site_url().'" />';
        $meta 		   .= '<meta property="og:type" content="website" />';
        $meta 		   .= '<meta property="og:title" content="'.$title.'" />';
        $meta 		   .= '<meta property="og:description" content="'.$description.'" />';
        $meta 		   .= '<meta property="og:image" content="'.base_url('image/logo/logo.png').'" />';
		return $meta;
	}

	public function index()
	{
		$user['userID'] = 1;
		$user['isBackend'] = TRUE;
		$this->session->set_userdata('user', $user);

        $data = array(
            'seo'     => $this->seo(),
            'menu'    => 'home',
            'header'  => 'header_dashboard',
            'content' => 'home',
            'footer'  => 'footer',
            'function'=>  array('home'),
        );
        $this->load->view('template/body', $data);
	}

	public function login()
	{
        $this->load->view('login2');
	}

	public function checklogin()
	{
		if ($this->input->post('username')) {
			$post_password = base64_encode($this->input->post('password'));
			$result0 = $this->db->get_where('users', array('email' => $this->input->post('username'), 'password' => $post_password, 'active' => 1, 'deactivate' => 1));
			$result = $result0->row_array();
			if($result0->num_rows()<1){
				header ('Content-type: text/html; charset=utf-8');
				print "<script type=\"text/javascript\">alert('ชื่อผุ้ใช้หรือรหัสผ่านไม่ถูกต้อง');</script>";
				redirect(site_url('home/login'), 'refresh');
			}else {
				$this->session->set_userdata('sess_login', $result);
				redirect(site_url('home/index'), 'refresh');
			}
		} else {
			redirect(site_url('home/login'), 'refresh');
		}
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect(site_url('home/login'), 'refresh');
	}

	public function country_ajax()
	{
        // echo $this->input->post('getID');
        $DBrecord['id'] = array('countryID' =>$this->input->post('getID'));
        $DBrecord['table'] = 'country_zone';
        $data['result'] = $this->DBrecord->get_result($DBrecord);
        echo '<option value="">-- City --</option>';
        foreach ($data['result'] as $key => $value) {
            echo '<option value="'.$value->zoneID.'">'.$value->title.'</option>';
        }
	}
	
	public function restaurant_ajax()
	{
        // echo $this->input->post('getID');
		$id['branchID'] = $this->input->post('branchID');
		$id['zoneID'] = $this->input->post('getID');
        $DBrecord['id'] = $id;
        $DBrecord['table'] = 'restaurant';
        $data['result'] = $this->DBrecord->get_result($DBrecord);
        echo '<option value="">-- Restaurant --</option>';
        foreach ($data['result'] as $key => $value) {
            echo '<option value="'.$value->restaurantID.'">'.$value->title.'</option>';
        }
	}
	
	public function forget() {
		// DBrecord //
		$input = $this->input->post();

		$newpassword = random_string('alnum', 6);
        $value['password'] = base64_encode($newpassword);

		$DBrecord['id'] = array('email' => $input['email']);
		$DBrecord['value'] = $value;
		$DBrecord['table'] = 'users';
		$this->DBrecord->update($DBrecord);
		// DBrecord //

        $this->sendmail($input['email'],$newpassword);
		redirect(site_url('home/login'), 'refresh');
	}

	private function sendmail($mails,$pass)
    {
        // Load PHPMailer library
        $this->load->library('phpmailer_lib');
        // PHPMailer object
        $mail = $this->phpmailer_lib->load();
        // SMTP configuration
        $mail->CharSet = 'UTF-8';
        $mail->isSMTP();
        $mail->SMTPDebug = 0;
        $mail->Host = "smtp.gmail.com";
        $mail->Port = "587";
        $mail->SMTPSecure = "tls";
        $mail->SMTPAuth = true;
        $mail->Username = "cocath2020@gmail.com";
        $mail->Password = "coca2020@pass";
        $mail->setFrom("cocath2020@gmail.com", "+inspired Community");
        // Set email format to HTML
        $mail->isHTML(true);
        // Add a recipient
        // $mail->addAddress('tarn_thanita@outlook.co.th');
        $mail->addAddress($mails);
        // Add cc or bcc 
        // $mail->addCC('cc@example.com');
        // $mail->addBCC('bcc@example.com');
        // Email subject
        $mail->Subject = 'Forget password - #'.date('dmYHis');

        $body = "<div style='border: 1px solid rgb(7, 133, 167);border-radius: 5px;color: #fff;'>";
		$body.= "<div style='background-color: rgb(0, 151, 192);padding: 10px;text-align: center;'>";
		$body.= "<p style='margin: 0;font-size: 20px;'>Forget password</p>";
		$body.= "</div>";
		$body.= "<div style='color: #000;padding: 10px;'>";
        $body.= "<p>Username :  <label style='color:#2156f3;font-weight: bold;'>".$mails."</label>";
        $body.= "<br>New Password :  <label style='color:#2156f3;font-weight: bold;'>".$pass."</label></p>";
        $body.= "</div>";
		$body.= "</div>";
        // Email body content
		$mail->Body = $body;
        
        // Send email
        if(!$mail->send()){
            echo 'Message could not be sent.';
            echo 'Mailer Error: ' . $mail->ErrorInfo;
        }else{
            echo 'success';        
		}
    }
    
}