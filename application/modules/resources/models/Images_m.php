<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Images_m extends CI_Model {


	public function __construct() {
		parent::__construct(); 
	}

	// Fetch records
	public function pagination_getData($rowno,$rowperpage) {
			
		$this->db->select('*');
		$this->db->from('resources_images');
        $this->db->limit($rowperpage, $rowno);  
		$query = $this->db->get();
       	
		return $query->result_array();
	}

	// Select total records
    public function pagination_getCount() {

    	$this->db->select('count(*) as allcount');
      	$this->db->from('resources_images');
      	$query = $this->db->get();
      	$result = $query->result_array();
      
      	return $result[0]['allcount'];
    }

}