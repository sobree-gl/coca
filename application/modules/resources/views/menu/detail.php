<!-- ============================================================== -->
<div class="page-wrapper">
    <div class="container-fluid">

        <div class="row">
            <div class="col-md-12">
                <h2>Resources / Menu</h2>
                <hr>
            </div>
        </div>

        <form id="Country" action="<?=current_url();?>" method="get">
            <div class="row">
                <div class="col-md-12">
                    <div class="listdata">
                        <div class="item-category">
                            <a href="<?=site_url('resources/menu');?>">
                                <div class="sub">Menu</div>
                            </a>
                            <?php
                            $this->db->from('branch');
                            $this->db->where('branchID', $this->uri->segment(4));
                            $result = $this->db->get();
                            foreach ($result->result_array() as $value) {
                            ?>
                            <div class="sub active"><?=$value['title'];?></div>
                            <?php } ?>
                            <div class="select">
                                <select class="form-control" name="countryID"
                                    change-country="<?=site_url('home/country_ajax');?>">
                                    <option value="" hidden>-- Country --</option>
                                    <?php
                                    $this->db->from('country');
                                    $country = $this->db->get();
                                    foreach ($country->result() as $value) {
                                    ?>
                                    <?php if ($value->countryID==$this->input->get('countryID')) { ?>
                                    <option value="<?=$value->countryID;?>" selected><?=$value->title;?></option>
                                    <?php }else{ ?>
                                    <option value="<?=$value->countryID;?>"><?=$value->title;?></option>
                                    <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="select">
                                <select class="form-control" name="zoneID"
                                    change-zone="<?=site_url('home/restaurant_ajax');?>" id="respon-zone">
                                    <option value="" hidden>-- City --</option>
                                    <?php
                                    $this->db->where('countryID', $this->input->get('countryID'));
                                    $this->db->from('country_zone');
                                    $country_zone = $this->db->get();
                                    foreach ($country_zone->result() as $value) {
                                    ?>
                                    <?php if ($value->zoneID==$this->input->get('zoneID')) { ?>
                                    <option value="<?=$value->zoneID;?>" selected><?=$value->title;?></option>
                                    <?php }else{ ?>
                                    <option value="<?=$value->zoneID;?>"><?=$value->title;?></option>
                                    <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="select">
                                <select class="form-control" name="restaurantID" id="respon-restaurant">
                                    <option value="" hidden>-- Restaurant --</option>
                                    <?php
                                    $this->db->where('branchID', $this->input->get('branchID'));
                                    $this->db->where('zoneID', $this->input->get('zoneID'));
                                    $this->db->from('restaurant');
                                    $restaurant = $this->db->get();
                                    foreach ($restaurant->result() as $value) {
                                    ?>
                                    <?php if ($value->restaurantID==$this->input->get('restaurantID')) { ?>
                                    <option value="<?=$value->restaurantID;?>" selected><?=$value->title;?></option>
                                    <?php }else{ ?>
                                    <option value="<?=$value->restaurantID;?>"><?=$value->title;?></option>
                                    <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                            <input type="hidden" id="branchID" name="branchID" value="<?=$this->uri->segment(4);?>">

                        </div>
                    </div>
                </div>
            </div>
        </form>

        <div class="listdata">

            <?php if ($this->session->sess_login['type']=='Corporate') { ?>
            <?php if (!empty($this->input->get('restaurantID'))) { ?>
            <div class="text-right">
                <a href="<?=$Urladd;?>">
                    <button type="button" class="btn btn-success"><i class="fas fa-plus"></i> Add</button>
                </a>
            </div>
            <?php } ?>
            <?php } ?>
            <table class="table">
                <thead>
                    <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                    $this->db->where('countryID', $this->input->get('countryID'));
                    $this->db->where('zoneID', $this->input->get('zoneID'));
                    $this->db->where('branchID', $this->input->get('branchID'));
                    $this->db->where('restaurantID', $this->input->get('restaurantID'));
                    $this->db->from('resources_menu');
                    $resources_menu = $this->db->get()->result();
                    if(!empty($resources_menu)){
                    foreach ($resources_menu as $value) {
                    ?>
                    <tr>
                        <td style="width: 130px;">
                            <div class="tr-col">
                                <img src="<?=base_url('uploads/resources/menu/'.$value->image);?>" style="width: 100px;" alt="">
                            </div>
                        </td>
                        <td>
                            <div class="tr-col">
                                <p class="head"><?=$value->title;?></p>
                                <?=$value->detail;?>
                            </div>
                        </td>
                        <td>
                            <div class="tr-col text-right">
                                <?php if ($this->session->sess_login['type']=='Corporate') { ?>
                                <div class="mb-2">
                                    <a href="<?=$Urledit.'/'.$value->menuID;?>">
                                        <button type="button" class="btn btn-warning">Edit</button>
                                    </a>
                                    <a href="javascript:void(0);" click-delete="<?=$Urldelete.'/'.$value->menuID;?>">
                                        <button type="button" class="btn btn-danger">Delete</button>
                                    </a>
                                </div>
                                <?php } ?>
                                <a href="<?=base_url('uploads/resources/menu/'.$value->file_doc);?>" target="_blank">
                                    <button type="button" class="btn btn-success">Download</button>
                                </a>
                            </div>
                        </td>
                    </tr>
                    <?php  } }else{ ?>
                    </tr>
                    <div class="col-12 mt-3">
                        <div class="alert alert-warning text-center">
                            <strong>! </strong> Please select from drop down list.

                        </div>
                    </div>
                    </tr>
                    <?php } ?>

                </tbody>
            </table>

        </div>

        <!-- ============================================================== -->
    </div>
</div>