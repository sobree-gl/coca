<!-- fancybox3 -->
<script type="text/javascript" src="<?=base_url('plugin/fancybox3/dist/jquery.fancybox.min.js');?>"></script>

<script>
$('select[change-country]').on('change', function(){
    $('body').loading();
    var sendURL = $(this).attr('change-country');
    var sendID = $(this).val();
    // alert(sendURL);
    if(sendID){
        $.ajax({
            type:'POST',
            url: sendURL,
            data:{ getID:sendID },
            success:function(html){
                $('#respon-zone').html(html);
                // alert(html);
                $('body').loading('stop');
            }
        }); 
    }
});

$('select[change-zone]').on('change', function(){
    $('body').loading();
    var sendURL = $(this).attr('change-zone');
    var sendID = $(this).val();
    var send_branchID = '<?=end($this->uri->segment_array());?>';
    // alert(sendID);
    if(sendID){
        $.ajax({
            type:'POST',
            url: sendURL,
            data:{ 
                getID:sendID,
                branchID:send_branchID
             },
            success:function(html){
                $('#respon-restaurant').html(html);
                // alert(html);
                $('body').loading('stop');
            }
        }); 
    }
});

$("#respon-restaurant").change(function () {
    if ($(this).val()) {
        $('form#Country').submit();
    }
});
</script>