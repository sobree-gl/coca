<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Images extends MX_Controller {

	public function __construct()
	{
        parent::__construct();
		// Load model
		$this->load->model('Images_m');
        $this->load->model('DBrecord');

	}

    private function seo()
	{
		$title          = "Resources / Images";
		$robots         = "noindex,nofollow";
		$description    = "titlewebtitleweb";
		$keywords       = "titleweb,titleweb";
		$meta  			= '<TITLE>'.$title.'</TITLE>';
		$meta 		   .= '<meta name="robots" content="'.$robots.'"/>';
		$meta		   .= '<meta name="description" content="'.$description.'"/>';
        $meta 		   .= '<meta name="keywords" content="'.$keywords.'"/>';
        $meta 		   .= '<meta property="og:url" content="'.site_url().'" />';
        $meta 		   .= '<meta property="og:type" content="website" />';
        $meta 		   .= '<meta property="og:title" content="'.$title.'" />';
        $meta 		   .= '<meta property="og:description" content="'.$description.'" />';
        $meta 		   .= '<meta property="og:image" content="'.base_url('image/logo/logo.png').'" />';
		return $meta;
	}

	public function index()
	{
        $data = array(
            'seo'     => $this->seo(),
            'menu'    => 'resources',
            'header'  => 'header',
            'content' => 'images/images',
            'footer'  => 'footer',
            'function'=>  array('resources'),
        );
        $this->load->view('template/body', $data);
    }
    
    public function detail()
	{
        $data = array(
            'seo'     => $this->seo(),
            'menu'    => 'resources',
            'header'  => 'header',
            'content' => 'images/images_detail',
            'footer'  => 'footer',
            'function'=>  array('resources'),
        );
        $this->load->view('template/body', $data);
    }

    public function multiple_upload()
    {
        if(isset($_FILES['filename']) && !empty($_FILES['filename']['name'])) {
            $filesCount = count($_FILES['filename']['name']);
            for($i = 0; $i < $filesCount; $i++){
                $_FILES['file']['name'] = $_FILES['filename']['name'][$i];
                $_FILES['file']['type'] = $_FILES['filename']['type'][$i];
                $_FILES['file']['tmp_name'] = $_FILES['filename']['tmp_name'][$i];
                $_FILES['file']['error'] = $_FILES['filename']['error'][$i];
                $_FILES['file']['size'] = $_FILES['filename']['size'][$i];
                $config['upload_path'] = './uploads/resources/images/'; 
                $config['allowed_types'] = 'jpg|jpeg|png|JPEG|JPG|PNG';
                $config['encrypt_name']  = TRUE;
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if($this->upload->do_upload('file')){
                    $fileData = $this->upload->data();

                    $this->load->library('image_lib');
                    $configer =  array(
                        'image_library'   => 'gd2',
                        'source_image'    =>  $fileData['full_path'],
                        'maintain_ratio'  =>  TRUE,
                        'width'           =>  900,
                        'height'          =>  900,
                    );
                    $this->image_lib->clear();
                    $this->image_lib->initialize($configer);
                    $this->image_lib->resize();

                    $uploadData[$i]['files'] = $fileData['file_name'];
                    $uploadData[$i]['countryID'] = $this->input->post('countryID');
                    $uploadData[$i]['zoneID'] = $this->input->post('zoneID');
                    $uploadData[$i]['branchID'] = $this->input->post('branchID');
                    $uploadData[$i]['restaurantID'] = $this->input->post('restaurantID');
                    $uploadData[$i]['createDate'] = date('Y-m-d H:i:s');
                }
            }
            if(!empty($uploadData)){
                $this->db->insert_batch('resources_images',$uploadData);
            }   
        }
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function delete()
	{
        // DBrecord //
        $DBrecord['id'] = array('imagesID' =>  end($this->uri->segment_array()));
        $DBrecord['table'] = 'resources_images';

        $result = $this->DBrecord->get_first($DBrecord);
        if ($result->files) {
            unlink('./uploads/resources/images/'.$result->files);
        }

        $this->DBrecord->delete($DBrecord);
        // DBrecord //
        redirect($_SERVER['HTTP_REFERER']);
	}

        
    public function loadRecord($rowno=0){

		// Row per page
		$rowperpage = 12;
		// Row position
		if($rowno != 0){
			$rowno = ($rowno-1) * $rowperpage;
		}
      	
      	// All records count
      	$allcount = $this->Images_m->pagination_getCount();
      	// Get  records
        $users_record = $this->Images_m->pagination_getData($rowno,$rowperpage);
          
        // with bootstrap 4
        $this->config->load('pagination_custom', TRUE);
        $config = $this->config->item('pagination_custom');
        
      	// Pagination Configuration
      	$config['base_url'] = $this->input->get('Url');
      	$config['use_page_numbers'] = TRUE;
		$config['total_rows'] = $allcount;
		$config['per_page'] = $rowperpage;
		// Initialize
		$this->pagination->initialize($config);
		// Initialize $data Array
		$data['pagination'] = $this->pagination->create_links();
		$data['result'] = $users_record;
        $data['row'] = $rowno;

		$data['loadRecordAjax'] = $this->loadRecordAjax($users_record);

		echo json_encode($data);
		
    }

    public function loadRecordAjax($users_record){
        // $result_view = print_r($result);
        $data['result'] = $users_record;
        return $this->load->view('images/images_ajax', $data, true);
    }


    
}