<!-- ============================================================== -->
<div class="page-wrapper">
    <div class="container-fluid">

        <div class="row">
            <div class="col-md-12">
                <h2>menu</h2>
                <hr>
                <div class="">
                    <a href="<?=$Urladd;?>">
                        <button type="button" class="btn btn-success"><i class="fas fa-plus"></i> Add</button>
                    </a>
                </div>

                <div class="listdata">
                <table class="table">
                    <thead>
                        <tr>
                            <th>menu</th>
                            <th class="text-center"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($result as $val) {
                        ?>
                        <tr>
                            <td>
                                <div class="tr-col"><?=$val->label;?></div>
                            </td>
                            <td>
                                <div class="tr-col text-right">
                                    <a href="<?=$Urledit.'/'.$val->subID;?>">
                                        <button type="button" class="btn btn-warning">Edit</button>
                                    </a>
                                    <a href="javascript:void(0);"
                                        click-delete="<?=$Urldelete.'/'.$val->subID;?>">
                                        <button type="button" class="btn btn-danger">Delete</button>
                                    </a>
                                </div>
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
                </div>

            </div>
        </div>

        <!-- ============================================================== -->
    </div>
</div>