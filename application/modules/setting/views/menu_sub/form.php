<!-- ============================================================== -->
<div class="page-wrapper">
    <div class="container-fluid">

        <div class="row">
            <div class="col-md-12">
                <h2>menu</h2>
                <hr><br>
                <?php echo form_open_multipart($Urlform); ?>
                    <div class="form-group">
                        <div class="row">
                            <label for="title" class="col-md-2 control-label">title</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" id="label" name="label" value="<?=isset($result->label) ? $result->label : '';?>">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="offset-md-2 col-md-10">
                                <input type="hidden" id="menuID" name="menuID" value="<?=$this->session->thisURL['id'];?>">
                                <input type="hidden" id="subID" name="subID" value="<?=isset($result->subID) ? $result->subID : '';?>">
                                <button type="submit" class="btn btn-info">SAVE</button>
                            </div>
                        </div>
                    </div>
                <?php echo form_close(); ?>
            </div>
        </div>

        <!-- ============================================================== -->
    </div>
</div>