<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('DBrecord');

        $this->table = 'users';
    }
    


    private function seo()
	{
		$title          = "Control system / Profile";
		$robots         = "noindex,nofollow";
		$description    = "titlewebtitleweb";
		$keywords       = "titleweb,titleweb";
		$meta  			= '<TITLE>'.$title.'</TITLE>';
		$meta 		   .= '<meta name="robots" content="'.$robots.'"/>';
		$meta		   .= '<meta name="description" content="'.$description.'"/>';
        $meta 		   .= '<meta name="keywords" content="'.$keywords.'"/>';
        $meta 		   .= '<meta property="og:url" content="'.site_url().'" />';
        $meta 		   .= '<meta property="og:type" content="website" />';
        $meta 		   .= '<meta property="og:title" content="'.$title.'" />';
        $meta 		   .= '<meta property="og:description" content="'.$description.'" />';
        $meta 		   .= '<meta property="og:image" content="'.base_url('image/logo/logo.png').'" />';
		return $meta;
    }

    private function SiteURL($SiteURL)
	{
		$SiteURL = site_url('setting/profile/'.$SiteURL);
        return $SiteURL;
	}
    
    private function thisURL()
	{
		$sess_data = array(
            'id' => null,
            'link' => current_url()
        );
        $this->session->set_userdata('profile',$sess_data);
    }

    private function upload_image($file, $path) {
		$config['upload_path'] = $path;
		$config['allowed_types'] = '*';
		$config['max_size'] = '*';
		$config['max_width']  = '*';
		$config['max_height']  = '*';
		$config['encrypt_name']  = TRUE;
		$this->load->library('upload', $config);
		if(!$this->upload->do_upload($file)) {
			return array('file_name' => false); // ถ้าอัพโหลดไม่ได้ ไม่สามารถเรียกดูข้อมูลไฟล์ที่อัพได้
		}else{
            $file_name = $this->upload->data();  // ถ้าอัพโหลดได้ เราสามารถเรียกดูข้อมูลไฟล์ที่อัพได้
            return $file_name['file_name'];
		}
	}

	public function index()
	{
        $this->thisURL();
        $data = array(
            'seo'     => $this->seo(),
            'menu'    => 'setting',
            'header'  => 'header',
            'content' => 'profile/index',
            'footer'  => 'footer',
            'function'=>  array('setting'),
        );
        // DBrecord //
        $DBrecord['table'] = $this->table;
        $data['result'] = $this->DBrecord->get_result($DBrecord);
        // DBrecord //
        $data['Urladd'] = $this->SiteURL('form/add');
        $data['Urledit'] = $this->SiteURL('form');
        $data['Urldelete'] = $this->SiteURL('delete');
        $this->load->view('template/body', $data);
    }
    
    public function form()
	{
        $this->thisURL();
        $data = array(
            'seo'     => $this->seo(),
            'menu'    => 'setting',
            'header'  => 'header',
            'content' => 'profile/form',
            'footer'  => 'footer',
            'function'=>  array('setting'),
        );

        $this->db->select('t1.title, t1.groupID');
        $this->db->from('users_group as t1');
        $this->db->join('users_access as t2', 't1.accessID = t2.accessID', 'LEFT');
        $data['result_users_group'] = $this->db->get()->result();

        // DBrecord //
        $DBrecord['id'] = array('usersID' =>  end($this->uri->segment_array()));
        $DBrecord['table'] = $this->table;
        $data['result'] = $this->DBrecord->get_first($DBrecord);
        // DBrecord //
        if (end($this->uri->segment_array())=='add') {
            $data['Urlform'] = $this->SiteURL('create');
        } else {
            $data['Urlform'] = $this->SiteURL('update');
        }
        
        $this->load->view('template/body', $data);
    }
    
    public function _build_data($input)
	{
        // $value['username'] = $input['username'];
        $value['title'] = $input['title'];
        $value['firstname'] = $input['firstname'];
        $value['lastname'] = $input['lastname'];
        $value['position'] = $input['position'];
        $value['password'] = base64_encode($input['password']);
        $value['email'] = $input['email'];
        $value['active'] = 1;

        if ($input['usersID']==null) {
            $value['createDate'] = date('Y-m-d H:i:s');
            $value['createBy'] = $this->session->sess_login['usersID'];
        } else {
            $value['updateDate'] = date('Y-m-d H:i:s');
            $value['updateBy'] = $this->session->sess_login['usersID'];
        }
        return $value;
    }

	public function create()
	{
        // DBrecord //
        $input = $this->input->post();
        $value = $this->_build_data($input);
        
        $value['image'] = $input['file_img_hid'];
		if (isset($_FILES['file_img']['name']) && !empty($_FILES['file_img']['name'])) {
            $value['image'] = $this->upload_image('file_img', './uploads/users/');
        }

        $DBrecord['value'] = $value;
        $DBrecord['table'] = $this->table;
        $this->DBrecord->insert($DBrecord);
        // // DBrecord //
        redirect( $this->session->profile['link'], 'refresh');
    }
    
    public function update()
	{
        // DBrecord //
        $input = $this->input->post();
        $value = $this->_build_data($input);

        $value['image'] = $input['file_img_hid'];
        if (isset($_FILES['file_img']['name']) && !empty($_FILES['file_img']['name'])) {
            $value['image'] = $this->upload_image('file_img', './uploads/users/');
            if ($value['image']) {
                unlink('./uploads/users/'.$input['file_img_hid']);
            }
        }

        $DBrecord['id'] = array('usersID'=>$input['usersID']);
        $DBrecord['value'] = $value;
        $DBrecord['table'] = $this->table;
        $this->DBrecord->update($DBrecord);
        // DBrecord //
        redirect( $this->session->profile['link'], 'refresh');
    }
    
    public function delete()
	{
        // DBrecord //
        $DBrecord['id'] = array('usersID' =>  end($this->uri->segment_array()));
        $DBrecord['table'] = $this->table;

        $result = $this->DBrecord->get_first($DBrecord);
        if ($result->image) {
            unlink('./uploads/users/'.$result->image);
        }
        
        $this->DBrecord->delete($DBrecord);
        // DBrecord //
        redirect( $this->session->profile['link'], 'refresh');
	}
    
}