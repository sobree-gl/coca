<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('DBrecord');
	}

    private function seo()
	{
		$title          = "Setting / Menu";
		$robots         = "noindex,nofollow";
		$description    = "titlewebtitleweb";
		$keywords       = "titleweb,titleweb";
		$meta  			= '<TITLE>'.$title.'</TITLE>';
		$meta 		   .= '<meta name="robots" content="'.$robots.'"/>';
		$meta		   .= '<meta name="description" content="'.$description.'"/>';
        $meta 		   .= '<meta name="keywords" content="'.$keywords.'"/>';
        $meta 		   .= '<meta property="og:url" content="'.site_url().'" />';
        $meta 		   .= '<meta property="og:type" content="website" />';
        $meta 		   .= '<meta property="og:title" content="'.$title.'" />';
        $meta 		   .= '<meta property="og:description" content="'.$description.'" />';
        $meta 		   .= '<meta property="og:image" content="'.base_url('image/logo/logo.png').'" />';
		return $meta;
    }

    private function SiteURL($SiteURL)
	{
		$SiteURL = site_url('setting/menu/'.$SiteURL);
        return $SiteURL;
	}
    
    private function thisURL()
	{
		$sess_data = array(
            'id' => null,
            'link' => current_url()
        );
        $this->session->set_userdata('thisURL',$sess_data);
	}

	public function index()
	{
        $this->thisURL();
        $data = array(
            'seo'     => $this->seo(),
            'menu'    => 'setting',
            'header'  => 'header',
            'content' => 'menu/index',
            'footer'  => 'footer',
            'function'=>  array('setting'),
        );
        // DBrecord //
        $DBrecord['table'] = 'menu';
        $data['result'] = $this->DBrecord->get_result($DBrecord);
        // DBrecord //
        $data['Urlform'] = $this->SiteURL('form');
        $data['Urldelete'] = $this->SiteURL('delete');
        $data['Urlaction'] = site_url('setting/menu_sub/index');
        $this->load->view('template/body', $data);
    }
    
    public function form()
	{
        $data = array(
            'seo'     => $this->seo(),
            'menu'    => 'setting',
            'header'  => 'header',
            'content' => 'menu/form',
            'footer'  => 'footer',
            'function'=>  array('setting'),
        );
        // DBrecord //
        $DBrecord['id'] = array('menuID' =>  end($this->uri->segment_array()));
        $DBrecord['table'] = 'menu';
        $data['result'] = $this->DBrecord->get_first($DBrecord);
        // DBrecord //
        if ($data['result']->menuID==null) {
            $data['Urlform'] = $this->SiteURL('create');
        } else {
            $data['Urlform'] = $this->SiteURL('update');
        }
        
        $this->load->view('template/body', $data);
    }
    
    public function _build_data($input)
	{
        $value['label'] = $input['label'];

        // if ($input['menuID']==null) {
        //     $value['createDate'] = date('Y-m-d H:i:s');
        //     $value['createBy'] = $this->session->sess_login['usersID'];
        // } else {
        //     $value['updateDate'] = date('Y-m-d H:i:s');
        //     $value['updateBy'] = $this->session->sess_login['usersID'];
        // }
        return $value;
    }

	public function create()
	{
        // DBrecord //
        $input = $this->input->post();
        $value = $this->_build_data($input);
        $DBrecord['value'] = $value;
        $DBrecord['table'] = 'menu';
        $this->DBrecord->insert($DBrecord);
        // DBrecord //
        redirect( $this->session->thisURL['link'], 'refresh');
    }
    
    public function update()
	{
        // DBrecord //
        $input = $this->input->post();
        $value = $this->_build_data($input);
        $DBrecord['id'] = array('menuID'=>$input['menuID']);
        $DBrecord['value'] = $value;
        $DBrecord['table'] = 'menu';
        $this->DBrecord->update($DBrecord);
        // DBrecord //
        redirect( $this->session->thisURL['link'], 'refresh');
    }
    
    public function delete()
	{
        // DBrecord //
        $DBrecord['id'] = array('menuID' =>  end($this->uri->segment_array()));
        $DBrecord['table'] = 'menu';
        $this->DBrecord->delete($DBrecord);
        // DBrecord //
        redirect( $this->session->thisURL['link'], 'refresh');
	}
    
}